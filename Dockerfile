# Build frontend application
FROM node:12-alpine as build-stage
ENV OUTPUT_DIR=dist
WORKDIR /app
COPY frontend/package*.json ./
RUN npm install
COPY frontend/ .
RUN npm run build

# Production stage
FROM node:12-alpine as production-stage
# Create required paths
RUN mkdir -p /run/nginx
RUN mkdir -p /etc/ssl/certs
# Install NGINX
RUN apk update && apk add nginx
# Copy NGINX configuration and SSL certificates
COPY deployment/nginx.conf /etc/nginx/nginx.conf
# Copy SSL certificates
COPY deployment/server.crt /etc/nginx/server.crt
COPY deployment/server.key /etc/nginx/server.key
COPY deployment/rootca.pem /etc/nginx/rootca.pem
# Node
WORKDIR /app
COPY backend .
COPY backend/.env .env
RUN npm install
RUN npm run build
COPY --from=build-stage /app/dist /app/public
EXPOSE 80
EXPOSE 443
COPY deployment/run.sh .
RUN chmod +x run.sh
CMD ["./run.sh"]
