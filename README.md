# Auction.Live
## Repository Structure
* `backend`: Node / Express backend application code
* `frontend`: Vue JS frontend code
* `deployment`: Build resources for docker builds (nginx configuration and run script)
* `Dockerfile`: Build resource to create the application container
* `docker-compose.yml`: docker-compose configuration for local testing purposes
* `tests`: Cypress E2E testing solution
* `docs`: Documentation
* `cert`: Resources for working with SSL in development

## Backend 
The backend is running a Node 12 environment with Express as the main framework. Typescript support is enabled. WebSockets will likely be handled using `socket.io`. All commands stated in this section are meant to be run from within the `backend` folder, not the repository root.

### Structure
* `build`: Build target for ts --> js transpiled files
* `public`: Build target for frontend during development. Files in this repository are served on the `/` route.
* `src`: Source code
* `.env.sample.dev/local/prod`: .env sample files
* `package.json`: Dependency definitions, location of build scripts etc.
* `tsconfig.json`: Typescript config

### Requirements / before running the backend application
* Node 12
* npm
* Run `npm install` in the backend directory
* Create .env file in local directory (e.g. `cp .env.sample.local .env`) and adjust required settings for given environment. For local development, the environment needs to be set to local.

### Run Database Migrations 
Before running the backend (in development or production) make sure the migrations are run to keep the database up to date. This can be done by running `npm run build` followed by `npm run migrate`. The project needs to be build first, as the migrations rely on the transpiled Javascript files. For now the database connection needs to be entered into the `.env` file in the backend folder. 

### Run Backend 
To run the backend in development run `npm run dev`. The backend runs on port 5500.

### Run Backend in production setting
To run the backend in the production setting without auto reload etc. first build the project `npm run build` and then run `npm run production`. The application will run on port 5500. TLS termination should be handled by a reverse proxy (this should be included in Plesks Node environment - however this remains to be tested, for the Docker setup this will be included in the image).

### Swagger Docs
The Swagger API documentation is served on the `/api-docs` route.

### Test Sockets
A test page for socket connections is served on the `/socketClient` route.

## Database
### Run a local MySQL instance
To spin up a local MYSQL instance for testing navigate into the `/mysql` directory and create a `.env` file from the sample to provide the password. Finally run `docker-compose up` in that directory. With the current setup, this will expose a MYSQL DB on port 3306.
You can then run the necessary migrations as described in the backend section of this document.

## Frontend 
The frontend is set up for Vue.js with Typescript support, Vue Router (no history mode), (potentially) Vuex, Babel, ESLint (standard config) and Cypress for E2E testing. WebSockets will likely be handled using `socket.io`.
The frontend folder in this structure represents both the 'admin', as well as the 'user' frontend as those share resources. 

### Structure 
* `dist`: optional build target for Vue (will be used as build target in the Docker based build)
* `public`: Vue public files
* `src`: main source code directory for Vue
* `package.json`: dependency and script configuration
* `tsconfig.json`: typescript configuration
* `vue.config.js`: Vue config (includes customizations for build target and SSL support during development)

### Requirements / before running the application
* Node 12
* npm
* Run `npm install` in the frontend directory

### Run Frontend
To run the frontend in development run `npm run serve`. This will run the app on port 8080.

### Build Frontend
To build the frontend locally (e.g. for testing within the backend as the serving layer) run `npm run build`. This will build the artifacts in the public folder of the backend. The target directory can be customized by setting the environment variable `OUTPUT_DIR`.

## Testing
Rudimentary E2E tests are set up in the `/tests` directory using Cypress.

### Structure
* `cypress`
  * `integration`: Folder for test specifications (structured using subfolders) - contains `*.spec.js` files
  * `plugins`: Can be used to use cypress plugins - empty for now
  * `screenshots`: Location for screenshots of errors (not checked in)
  * `videos`: Location for videos of errors (not checked in)
  * `support`: Bootstrapping artifact, can e.g. be used to define additional cypress commands
* `cypress.json`: Base configuration
* `package.json`: Node dependencies

### Open Cypress UI to see all available tests / run them manually
Navigate into the `/tests` directory. Ensure you have run `npm install` in that directory.
```sh
npm run cypress:open
```

### Run all Cypress tests
```sh
npm run cypress:run
```

## Application structure / routing schema
* `https://<tld>.<extension>`
* `/`: user frontend
* `/admin`: admin frontend
* `/api`: backend
* `/api-docs`: Swagger documentation

## Certificates / SSL
The clients wants to have rather fine grained control over encryption / his certificates, therefore we will develop with SSL enabled and in mind from the beginning. The `cert` folder in the repository root directory can be used to test the local environment for SSL support during development.

### Get SSL working locally
To get started easily with SSL support, the repository contains the required certificates for local development. The process for generating those is layed out below. To simply use the existing certificates you need your local environment to trust the `rootCA.pem` certificate. On Mac you can import this file into your Keychain and change the settings to trust it. In the future, certificates from trusted certificate authorities will be provided by the client.

### Generate certificates for local testing with SSL
#### Generate Root SSL Certificate
```sh
# Using passphrase 'auction'
openssl genrsa -des3 -out rootCA.key 2048
# Using passphrase 'auction' and Berlin Bytes info
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -out rootCA.pem
```
For local development make sure you trust rootCA.pem in your local environment.

#### Domain SSL Certificate
```sh
# Using 'server.csr.cnf' create a server key and the certificate signing request
openssl req -new -sha256 -nodes -out server.csr -newkey rsa:2048 -keyout server.key -config <( cat server.csr.cnf )
# Sign certificate
openssl x509 -req -in server.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out server.crt -days 500 -sha256 -extfile v3.ext
```

#### File Summary
* `rootCA.key`: Root CA Key
* `rootCA.pem`: Root CA Certificate
* `rootCA.srl`: Keeps track of number of signed certificates
* `server.crt`: Server certificate that needs to be used in the application
* `server.csr`: Server certificate signing request, can be ignored after it has been signed
* `server.key`: Server certificate key that needs to be used in the application. 

## Run project dockerized locally
This is handled using the docker-compose file in the root directory of the project. 

## Build project for deployment
The project will be deployed as a Node 12 http server running in a docker container. The container can be built using the `Dockerfile` in the repository root directory. The build has two stages - the first stage builds the frontend, the second stage is the final deployment image. We are using node:12-alpine as a base image. Inside the container, the node application runs on port 5500. An additional NGINX process is serving the ports 80 and 443 (which should be exposed) and handles TLS termination (and acts as a reverse proxy for the backend).
The configurations and certificates for the internal NGINX are added in the build stage, however, those can be overwritten using volumes (see examples below).

### Build image
```sh
# Build individual container
docker build . -t <image_name>

# Build using docker-compose
docker-compose build
```

### Run
```sh
# Run container individually
docker run \
  -it --rm \
  -p 80:80 -p 443:443 \
  -v $(pwd)/deployment/server.crt:/etc/ssl/certs/server.crt \
  -v $(pwd)/deployment/server.key:/etc/ssl/certs/server.key \
  -v $(pwd)/deployment/rootca.pem:/etc/ssl/certs/rootca.pem \
  -v $(pwd)/deployment/nginx.conf:/etc/nginx/nginx.conf \
  <image_name>

# Run with docker-compose
docker-compose up
```
