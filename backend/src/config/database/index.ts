export default ()=>{
    return {
        server: {
            useNewUrlParser: true,
            auto_reconnect: true,
            reconnectInterval: 30000, // milliseconds to retry connection
            reconnectTries: Infinity,
            socketOptions: {
                keepAlive: 10000,
                connectTimeoutMS: 50000,
            },
        },
        mysql: {
            name: process.env.DEFAULT_DATABASE_CONNECTION_NAME,
            type: process.env.DEFAULT_DATABASE_TYPE,
            host: process.env.DEFAULT_DATABASE_HOST,
            port: process.env.DEFAULT_DATABASE_PORT,
            username: process.env.DEFAULT_DATABASE_USERNAME,
            password: process.env.DEFAULT_DATABASE_PASSWORD,
            database: process.env.DEFAULT_DATABASE_NAME,
            entities: ['*.ts']
        }
    }
}
