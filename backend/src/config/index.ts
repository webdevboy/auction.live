import { default as uploadImage } from './uploadImage';
import { default as database } from './database';
import { default as email } from './email';
import { default as i18n } from './i18n';

export default () => {
    return {
        database: database(),
        uploadImage: uploadImage(),
        email: email(),
        i18n: i18n(),
    }
}
