import * as passport from 'passport';
import * as session from 'express-session'
import localStrategy from './strategies/local';

const passportStrategies =  app => {

    app.use(session({ secret: 'SECRET' })); // Session middleware, enable this if you are using twitter passport or any service requires OAuth 1.0
    app.use(passport.initialize());
    app.use(passport.session()); // Enable this if you are using twitter passport or any service requires OAuth 1.0

    // Init strategy for local authentication
    localStrategy();
};

export default passportStrategies;
