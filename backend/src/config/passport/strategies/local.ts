import * as passportLocal from 'passport-local';
import * as passport from 'passport';
import * as bcrypt from 'bcryptjs';

const LocalStrategy = passportLocal.Strategy;

export default () => {
  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
      },
      async (req, email, password, done) => {
        try {
            // Init models
            let {
                user: userModel
            } = await global.mysqlDb.repositories(['user'], global.env.defaultDb);
            const { body: data } = req;

            // Check invalid email
            const emailRegex = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if(!emailRegex.exec(email)){
                return done({
                    errors: ['email.invalid']
                },null);
            }

            // Find user by email
            let rs = await userModel
                .createQueryBuilder('users')
                .addSelect("users.password")
                .where({email})
                .getOne();

            // Check email existed or not
            if (!rs) {
                return done({
                    errors: ['resetPassword.email.not.existed']
                },null);
            }

            if(rs.status === '0'){
                return done({
                    errors: ['login.status.pending']
                },null);
            }

            if(rs.status === '1'){
                return done({
                    errors: ['login.status.locked']
                },null);
            }

            // Check if matched password
            const isMatch = await bcrypt.compare(password, rs.password);

            // Not matched password, callback error
            if (!isMatch) {
                return done({
                    errors: ['password.not.matched']
                }, null);
            }

            // Remove password property
            delete rs.password;

            return done(null, {
                data: rs
            });
        } catch (err) {
          return done(err, null);
        }
      }
    )
  );
};
