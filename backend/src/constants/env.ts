export default () => {
    return {
        basePath: process.env.BASE_PATH,
        jwtSecret: process.env.JWT_SECRET,
        defaultAccessUsername: process.env.DEFAULT_ACCESS_USERNAME,
        defaultAccessPassword: process.env.DEFAULT_ACCESS_PASSWORD,
        hashSalt: process.env.HASH_SALT,
        branchKey: process.env.BRANCH_KEY,
        webBaseUrl: process.env.WEB_BASE_URL,
        mode: process.env.NODE_ENV,
        defaultDb: process.env.DEFAULT_DATABASE_NAME
    }
};
