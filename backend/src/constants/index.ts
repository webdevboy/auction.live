import { default as env } from './env';

import { default as config } from '../config';
import { repositories } from '../models/mysql';
import MySql from '../services/mysql'
import Logger from '../services/logger';
import i18nService from '../services/i18n';

const initGlobalVariables = async () => {
    // Config data
    global.configs = config();
    // Env data
    global.env = env();
    // Logger service
    global.logger = new Logger();
    // i18n service
    global.i18n =  new i18nService(global.configs.i18n, global.env.mode);

    /*///////////////////////////////////////////////////////////////
   /////                START DATABASE CONFIG                  /////
   ///////////////////////////////////////////////////////////////*/


    const { database } = global.configs;

    // ------------------------- START MYSQL ---------------------------------------

    console.log('Start to connect to MySQL');
    global.mysqlInstance = new MySql(database.mysql);
    global.mysqlDb = { repositories, connections: {} };

    // Connect to mysql
    await global.mysqlInstance.createOrReuseConnection(global.env.defaultDb)

    // ------------------------- END MYSQL ---------------------------------------


    /*///////////////////////////////////////////////////////////////
    /////                  END DATABASE CONFIG                  /////
    ///////////////////////////////////////////////////////////////*/

    return global;

};

export default initGlobalVariables

