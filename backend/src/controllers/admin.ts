import { Request, Response, NextFunction } from 'express';
import {In} from "typeorm";
import * as moment from 'moment';
import * as _ from 'underscore';
import * as lodash from 'lodash';

import { jwtHelper, helpers } from '../utils';

import ImageService from '../services/image';
import HttpResponse from '../services/response';
import AuctionController from '../controllers/auction'

export default class AdminController {
    /**
     * @swagger
     * tags:
     *   - name: Admin
     *     description: Admin API
     */

    /**
     * @swagger
     * /v1/admin/user/list:
     *   get:
     *     description: Get user list API
     *     tags: [Admin]
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async getUserList(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Init user model
            const {
                user: userModel
            } = await global.mysqlDb.repositories(['user'], global.env.defaultDb);

            // Get resource data
            let rs = await userModel.find({role: 'user'});

            return HttpResponse.returnSuccessResponse(res, rs)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }

    };

    /**
     * @swagger
     * /v1/admin/user/activate/{userId}:
     *   put:
     *     description: Activate user
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: userId
     *         description: User Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */
    static async activateUser(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            // Init user model
            const {
                user: userModel
            } = await global.mysqlDb.repositories(['user'
            ], global.env.defaultDb);

            const { userId } = req.params;

            // Find the correct user
            let userData = await userModel.findOne({id: userId});
            console.log(userData)

            if(!userData){
                return HttpResponse.returnNotFoundResponse(res, 'admin.activate.user.not.found')
            }

            // Update user
            await userModel.update(userId, {status: '2'});

            return HttpResponse.returnSuccessResponse(res, 'Success')

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    };

    /**
     * @swagger
     * /v1/admin/user/{userId}:
     *   delete:
     *     description: Delete user
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: userId
     *         description: User Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */
    static async deleteUser(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            // Init user model
            const {
                user: userModel
            } = await global.mysqlDb.repositories(['user'
            ], global.env.defaultDb);

            const { userId } = req.params;

            // Get resource data
            let rs = await userModel.delete(userId);

            return HttpResponse.returnSuccessResponse(res, 'Success')

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    };

    /**
     * @swagger
     * definitions:
     *   UpdateProfile:
     *     required:
     *       - first_name
     *       - last_name
     *       - email
     *     properties:
     *       first_name:
     *         type: string
     *         description: First name
     *       last_name:
     *         type: string
     *         description: Last name
     *       email:
     *         type: string
     *         description: Email
     *
     */

    /**
     * @swagger
     * /v1/admin/user/{userId}:
     *   put:
     *     description: Update user profile
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: userId
     *         description: User Id
     *         required: true
     *         type: string
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/UpdateProfile'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */


    static async updateUser(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const { body: data } = req;
            const { userId } = req.params;

            // Init user model
            const {
                user: userModel
            } = await global.mysqlDb.repositories(['user'], global.env.defaultDb);

            // Check invalid email
            const emailRegex = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if(!emailRegex.exec(data.email)){
                return HttpResponse.returnBadRequestResponse(res, 'email.invalid')
            }

            let initData = lodash.omitBy({
                ...data
            }, lodash.isUndefined);

            // Update
            let rs = await userModel.update(userId, initData);

            return HttpResponse.returnSuccessResponse(res, 'Success');

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    };


    /**
     * @swagger
     * definitions:
     *   Auction:
     *     required:
     *       - code
     *       - active_until
     *     properties:
     *       code:
     *         type: string
     *       active_until:
     *         type: string
     */

    /**
     * @swagger
     * /v1/admin/auction:
     *   post:
     *     description: Create auction
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/Auction'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async createAuction(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const { body: data } = req;

            // Check invalid time
            let date = new Date(data.active_until);

            // Invalid date
            if(date.toString() === 'Invalid Date'){
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.untilDate.invalid')
            }


            let insertData = await AuctionController.createAuctionByUser(req['userProfile'].id, new Date(data.active_until));

            return HttpResponse.returnSuccessResponse(res, insertData);

        }catch (e) { // Pass error to the next middleware
            next(e)
        }
    };

    /**
     * @swagger
     * /v1/admin/auction/list:
     *   get:
     *     description: Get auction list API
     *     tags: [Admin]
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async getAuctionList(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Get data
            let rs = await AuctionController.getAuctionList();

            return HttpResponse.returnSuccessResponse(res, rs)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }
    };


    /**
     * @swagger
     * /v1/admin/auction/start/{id}:
     *   put:
     *     description: Start auction API
     *     tags: [Admin]
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: id
     *         description: Auction Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async startAuction(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const { id } = req.params;

            //Init model
            const {
                auctions: auctionModel,
                articles: articleModel,
                bid_steps: bidStepModel
            } = await global.mysqlDb.repositories(['auctions', 'articles', 'bid_steps'], global.env.defaultDb);

            // Check if there is any auction has started
            let startedAuctions = await auctionModel.find({status: In(['started', 'paused'])});

            // Return error
            if(startedAuctions.length > 0){
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.start.existed')
            }

            // Check if there is at lease 1 available item to join auction
            let items = await articleModel.find({auction_id: id, status: '1'});

            // Check if bid step have already set
            let bidStep = await bidStepModel.find({auction_id: id});

            if(bidStep.length === 0){
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.bidStep.require')
            }

            // Return error
            if(items.length === 0){
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.item.not.ready')
            }

            // Start auction with the first available articles
            await articleModel.update({id: items[0].id, status: '1'}, {status: 2});

            // Get data
            let rs = await AuctionController.startAuction(parseInt(id));

            // Get full data
            let auctionData = await AuctionController.getFullDetailCurrentBid();


            // Send socket event to all connected user to update bid
            global.socket.sendNewEventToAllConnectedUser('updateBid', auctionData);

            return HttpResponse.returnSuccessResponse(res, rs)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }
    };

    /**
     * @swagger
     * definitions:
     *   DoAuction:
     *     required:
     *       - type
     *     properties:
     *       type:
     *         type: string
     *         enum:
     *          - pause
     *          - resume
     *          - stop
     *          - bidWarning
     *          - sellItem
     *          - hallBid
     *          - undo
     *          - askBidValue
     *       bidderNumber:
     *         type: number
     *       expectPrice:
     *         type: number
     */

    /**
     * @swagger
     * /v1/admin/auction/doAction:
     *   put:
     *     description: Do auction action
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/DoAuction'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async doAction(req: Request, res: Response, next: NextFunction): Promise<any>{
        try {
            const { body: data } = req;

            switch (data.type){
                case 'pause': {
                    await AuctionController.pauseAuction();

                    // Send for all user about this update
                    global.socket.sendNewEventToAllConnectedUser('pause', 'No live auction is in progress at the moment.');
                    return HttpResponse.returnSuccessResponse(res, null);
                }

                case 'resume': {
                    await AuctionController.resumeAuction();

                    // Send for all user about this update
                    global.socket.sendNewEventToAllConnectedUser('resume', null);
                    return HttpResponse.returnSuccessResponse(res, null);
                }

                case 'bidWarning': {
                    // Send for all user about this update
                    global.socket.sendNewEventToAllConnectedUser('bidWarning', 'You should place your bids quickly, as the article is about to be sold.');
                    return HttpResponse.returnSuccessResponse(res, null);
                }

                case 'stop': {
                    await AuctionController.stopAuction();

                    // Send for all user about this update
                    global.socket.sendNewEventToAllConnectedUser('stop', null);
                    return HttpResponse.returnSuccessResponse(res, null);
                }

                case 'sellItem': {
                    // Get the next item
                    let rs = await AuctionController.sellItem(res);

                    // All items in this auction has been sold
                    if(!rs){
                        // Stop current auction
                        await AuctionController.stopAuction();

                        // Send for all user that current auction has been stopped
                        global.socket.sendNewEventToAllConnectedUser('stop', null);
                    }else{
                        // Send for all user about this update
                        global.socket.sendNewEventToAllConnectedUser('sellItem', rs.data);
                    }


                    return HttpResponse.returnSuccessResponse(res, null);
                }

                case 'hallBid': {
                    // Get the next item
                    let rs = await AuctionController.sellItem(res,true, data.bidderNumber);

                    // All items in this auction has been sold
                    if(!rs){
                        // Stop current auction
                        await AuctionController.stopAuction();

                        // Send for all user that current auction has been stopped
                        global.socket.sendNewEventToAllConnectedUser('stop', null);
                    }else{
                        // Send for all user about this update
                        global.socket.sendNewEventToAllConnectedUser('sellItem', rs.data);
                    }


                    return HttpResponse.returnSuccessResponse(res, null);
                }

                case 'undo': {
                    // Get the next item
                    let rs = await AuctionController.undoBid();


                    return HttpResponse.returnSuccessResponse(res, rs);
                }

                case 'askBidValue': {
                    // Ask value
                    let rs = await AuctionController.askBid(res, data.expectPrice);

                    // Get full data
                    let auctionData = await AuctionController.getFullDetailCurrentBid();


                    // Send socket event to all connected user to update bid
                    global.socket.sendNewEventToAllConnectedUser('updateBid', auctionData);


                    return HttpResponse.returnSuccessResponse(res, auctionData);
                }
            }
        } catch (e) {
            next(e)
        }
    }

    /**
     * @swagger
     * /v1/admin/auction/{id}:
     *   get:
     *     description: Get auction detail API
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: id
     *         description: Auction Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */
    static async getAuctionDetail(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const { id } = req.params;

            // Find the correct user
            let auctionData = await AuctionController.getAuctionDetail(id);

            return HttpResponse.returnSuccessResponse(res, auctionData)

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    };

    /**
     * @swagger
     * /v1/admin/auction/jumpToArticle/{id}:
     *   put:
     *     description: Jump to specific article
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: id
     *         description: Article id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */
    static async jumpArticle(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const { id } = req.params;

            //Init model
            const {
                auctions: auctionModel,
                articles: articleModel,
                bids: bidModel
            } = await global.mysqlDb.repositories(['auctions', 'articles', 'bids'], global.env.defaultDb);

            // Get article detail
            let articleDetail = await articleModel.findOne({id})

            // Invalid article
            if(!articleDetail){
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.article.invalid')
            }

            // If there is any selling article, skip it
            await articleModel.update({status: '2'}, {status: '3'})

            // Jump to article
            await articleModel.update({id}, {status: '2'})

            // Stop current auction
            await auctionModel.update({status: 'started'}, {status: 'preparing'})

            return HttpResponse.returnSuccessResponse(res, articleDetail)

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    };

    /**
     * @swagger
     * /v1/admin/auction/{id}:
     *   delete:
     *     description: Delete auction detail API
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: id
     *         description: Auction Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */
    static async deleteAuction(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const { id } = req.params;

            // Find the correct user
            let auctionData = await AuctionController.deleteAuction(id);

            return HttpResponse.returnSuccessResponse(res, auctionData)

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    };


    /**
     * @swagger
     * definitions:
     *   BidStep:
     *     required:
     *       - limit
     *       - bid_step
     *     properties:
     *       limit:
     *         type: number
     *       bid_step:
     *         type: number
     */

    /**
     * @swagger
     * /v1/admin/auction/{id}/bid-step:
     *   post:
     *     description: Add bid step to specific auction
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: id
     *         description: Auction Id
     *         required: true
     *         type: string
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/BidStep'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async addBidStep(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const { body: data } = req;
            const { id } = req.params;

            // Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'], global.env.defaultDb);

            // TODO: Validation
            // Find the auction
            let auction = auctionModel.findOne({id});

            // Auction does not exist
            if(!auction){
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.not.existed')
            }

            let insertData = await AuctionController.addBidStepToAuction(parseInt(id), data);

            return HttpResponse.returnSuccessResponse(res, insertData);

        }catch (e) { // Pass error to the next middleware
            next(e)
        }
    };

    /**
     * @swagger
     * /v1/admin/auction/{id}/bid-step:
     *   get:
     *     description: Get bid step list API
     *     tags: [Admin]
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: id
     *         description: Auction Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async getBidStepList(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const { id } = req.params;

            // Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'], global.env.defaultDb);

            // TODO: Validation
            // Find the auction
            let auction = auctionModel.findOne({id});

            // Auction does not exist
            if(!auction){
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.not.existed')
            }

            // Get data
            let rs = await AuctionController.getBidStepList(parseInt(id));

            return HttpResponse.returnSuccessResponse(res, rs)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }
    };

    /**
     * @swagger
     * /v1/admin/auction/bid-step/{id}:
     *   delete:
     *     description: Delete bidStep API
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: id
     *         description: Bid step Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */
    static async deleteBidStep(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const {id} = req.params;

            // Find the correct user
            let auctionData = await AuctionController.deleteBidStep(parseInt(id));

            return HttpResponse.returnSuccessResponse(res, auctionData)

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    }

    /**
     * @swagger
     * definitions:
     *   Article:
     *     required:
     *       - auction_id
     *       - code
     *       - name
     *       - description
     *       - limit
     *       - estimated_price
     *     properties:
     *       auction_id:
     *         type: number
     *       code:
     *         type: string
     *       name:
     *         type: string
     *       description:
     *         type: string
     *       limit:
     *         type: number
     *       estimated_price:
     *         type: number
     */


    /**
     * @swagger
     * /v1/admin/article:
     *   post:
     *     description: Create article
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/Article'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async createArticle(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const { body: data } = req;

            // Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'], global.env.defaultDb);

            // TODO: Validation
            // Find the auction
            let auction = await auctionModel.findOne({id: data.auction_id});


            // Auction does not exist
            if(!auction){
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.not.existed')
            }

            let insertData = await AuctionController.createArticle(parseInt(data.auction_id), data);

            return HttpResponse.returnSuccessResponse(res, insertData);

        }catch (e) { // Pass error to the next middleware
            next(e)
        }
    };

    /**
     * @swagger
     * /v1/admin/article/{auctionId}/list:
     *   get:
     *     description: Get bid article list API
     *     tags: [Admin]
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: auctionId
     *         description: Auction Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async getArticleList(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const { auctionId } = req.params;

            // Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'], global.env.defaultDb);

            // TODO: Validation
            // Find the auction
            let auction = auctionModel.findOne({id: auctionId});

            // Auction does not exist
            if(!auction){
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.not.existed')
            }

            // Get data
            let rs = await AuctionController.getArticleList(parseInt(auctionId));

            return HttpResponse.returnSuccessResponse(res, rs)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }
    };

    /**
     * @swagger
     * /v1/admin/article/{id}:
     *   get:
     *     description: Get article API
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: id
     *         description: Article Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *     security:
     *          - auth: []
     */

    static async getArticleDetail(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const {id} = req.params;

            // Find the correct user
            let auctionData = await AuctionController.getArticleDetail(parseInt(id));

            return HttpResponse.returnSuccessResponse(res, auctionData ? auctionData : null)

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    }

     /**
     * @swagger
     * /v1/admin/article/{id}:
     *   delete:
     *     description: Delete article API
     *     tags: [Admin]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
      *         in: header
      *         required: false
      *         description: Language key
      *         type: string
      *         enum:
      *              - en
      *              - de
      *         default: de
     *       - in: path
     *         name: id
     *         description: Article Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */
    static async deleteArticle(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const {id} = req.params;

            // Find the correct user
            let auctionData = await AuctionController.deleteArticle(parseInt(id));

            return HttpResponse.returnSuccessResponse(res, auctionData)

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    }

}

