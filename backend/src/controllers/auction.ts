import { helpers } from '../utils';
import HttpResponse from "../services/response";
import {NextFunction, Request, Response} from "express";
import {MoreThan} from "typeorm";
import ImageService from "../services/image";
export default class AuctionController {

    /**
     * @swagger
     * tags:
     *   - name: Auction
     *     description: Auction API
     */


    /**
     * @swagger
     * /v1/auction/active:
     *   get:
     *     description: Get current active auction API
     *     tags: [Auction]
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async getCurrentActiveAuction(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            let rs = await AuctionController.getFullDetailCurrentBid(req['userProfile'].role === 'admin');

            // Init user model
            const {
                user: userModel,
                bids: bidModel
            } = await global.mysqlDb.repositories(['user', 'bids'], global.env.defaultDb);

            // If current user is admin, return user analytic data
            if(req['userProfile'].role === 'admin'){
                let userNumber = await userModel
                    .createQueryBuilder("users")
                    .select("count(*)", "count")
                    .where("users.role = :role", { role: 'user' })
                    .getRawOne();

                rs.userData = {
                    online: global.socket.getOnlineUser(),
                    register: userNumber.count
                }

                rs.bidHistory = await bidModel
                    .createQueryBuilder("bids")
                    .leftJoinAndSelect("bids.auction_users_id", "auction_users")
                    .select('*')
                    .where("bids.articles_id = :id AND bids.type != 'expect'", { id: rs.ongoingArticle.id })
                    .orderBy('bids.bid', 'DESC')
                    .getRawMany();

                //rs.bidHistory = await bidModel.find({auction_id: rs.id, order: {bid: "DESC"},relations: ['auction_users_id']})
            }

            return HttpResponse.returnSuccessResponse(res, rs)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }
    };

    static async getFullDetailCurrentBid(getAll?: boolean){
        //Init model
        const {
            auctions: auctionModel,
            articles: articleModel,
            bids: bidModel,
            bid_steps: bidStepModel
        } = await global.mysqlDb.repositories(['auctions', 'articles', 'bids', 'bid_steps'], global.env.defaultDb);

        let rs = null

        if(getAll){
            rs =  await auctionModel.findOne();
        }else{
            rs =  await auctionModel.findOne({status: 'started'});
        }



        // No current action available
        if(!rs){
            return null
        }

        // Get article list of current auctionModel
        rs.articleList = await articleModel.find({auction_id: rs.id});

        let soldArticle = rs ? rs.articleList.filter((item)=>{
                return item.status === '2'
            }) : []
        // Get on going item
        rs.ongoingArticle = soldArticle[0] ? soldArticle[0] : []

        // There is not item to sold
        if(soldArticle.length === 0){
            return rs
        }

        // Get max bid
        let highestBid = await bidModel
            .createQueryBuilder("bids")
            .leftJoinAndSelect("bids.auction_users_id", "auction_users")
            .leftJoinAndSelect("auction_users.user_id", "users")
            .select('*')
            .addSelect("max(bids.bid)", "value")
            .where("bids.articles_id = :id AND bids.type != 'expect'", { id: rs.ongoingArticle.id })
            .groupBy('bids.id')
            .orderBy('MAX(bids.bid)', 'DESC')
            .getRawOne();

        rs.highestBid = highestBid ? highestBid : null

        // Get expect price if possible
        let expectPrice = await bidModel
            .createQueryBuilder("bids")
            .select('*')
            .where("bids.articles_id = :id", { id: rs.ongoingArticle.id })
            .groupBy('bids.id')
            .orderBy('MAX(bids.id)', 'DESC')
            .getRawOne();

        rs.expectPrice = expectPrice ? (expectPrice.type === 'expect' ? expectPrice.bid : null)  : null

        // Find bid step base on highest Bid
        if(rs.highestBid){
            rs.bidStep = await bidStepModel.findOne({auction_id: rs.id, limit: MoreThan(rs.highestBid.bid)});

            // Reach the maximum step, use the maximum one
            if(!rs.bidStep){
                rs.bidStep = await bidStepModel.findOne({auction_id: rs.id, order: {limit: "DESC"}})
            }

        }else{// There is no bid yet
            // Use article value as the highest bid
            rs.bidStep = await bidStepModel.findOne({auction_id: rs.id, limit: MoreThan(soldArticle[0].limit)});

            // Reach the maximum step, use the maximum one
            if(!rs.bidStep){
                rs.bidStep = await bidStepModel.findOne({auction_id: rs.id, order: {limit: "DESC"}})
            }

            //rs.bidStep = await bidStepModel.findOne({auction_id: rs.id, order: {limit: "ASC"}})
        }

        return rs;
    }


    /**
     * @swagger
     * definitions:
     *   DoBid:
     *     required:
     *       - bid
     *     properties:
     *       bid:
     *         type: number
     *         description: Bid number
     *
     */


    /**
     * @swagger
     * /v1/auction/doBid/{auctionId}/{articleId}:
     *   put:
     *     description: Do bid
     *     tags: [Auction]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: auctionId
     *         description: Auction Id
     *         required: true
     *         type: string
     *       - in: path
     *         name: articleId
     *         description: Article Id
     *         required: true
     *         type: string
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/DoBid'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async doBid(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            //Init model
            const {
                auctions: auctionModel,
                auction_users: auctionUserModel,
                bids: bidModel,
                articles: articleModel,
                notifications: notificationModel
            } = await global.mysqlDb.repositories(['auctions', 'articles', 'bids', 'auction_users', 'articles', 'notifications'], global.env.defaultDb);

            const { body: data } = req;
            const { auctionId, articleId } = req.params;


            // Get current running auction
            let auction = await auctionModel.findOne({id: auctionId, status: 'started'});

            // No auction has been started
            if(!auction) {
                return HttpResponse.returnBadRequestResponse(res, 'admin.auction.not.ready')
            }

            // Check if article is valid to bid
            let articleData = await articleModel.findOne({id: articleId, status: '2'});

            // No valid article
            if(!articleData) {
                return HttpResponse.returnBadRequestResponse(res, 'user.bid.article.not.found')
            }

            // Check if the price < article price
            if(data.bid < articleData.limit){
                return HttpResponse.returnNotFoundResponseWithMessage(res, `You have to bid with the price larger than ${articleData.limit}`)
            }

            // Get max bid
            let maxBid = await bidModel
                .createQueryBuilder("bids")
                .select('*')
                .addSelect("max(bids.bid)", "value")
                .where("bids.articles_id = :id AND bids.type != 'expect'", { id: articleId })
                .groupBy('bids.id')
                .orderBy('MAX(bids.bid)', 'DESC')
                .getRawOne();

            if(maxBid && (maxBid.value > data.bid)){
                return HttpResponse.returnNotFoundResponseWithMessage(res, `You have to bid with the price larger than the highest price:  ${maxBid.value}`)
            }

            // Do bid
            await AuctionController.createNewBid({
                userId: req['userProfile'].id,
                auction_id: auctionId,
                articles_id: articleId,
                bid: data.bid
            });

            // Get full data
            let rs = await AuctionController.getFullDetailCurrentBid();

            // This user is currently person who has the highest bid
            if(rs.highestBid.user_id === req['userProfile'].id){
                // Add notification
                await notificationModel.save({
                    article: articleId,
                    to_user: req['userProfile'].id,
                    message: 'You are currently the highest bidder!',
                    sent_time: new Date()
                });

                // Send notification
                global.socket.sendNewEventToUsers([req['userProfile'].id], 'notification', 'You are currently the highest bidder!');
            }

            // Send socket event to all connected user to update bid
            global.socket.sendNewEventToAllConnectedUser('updateBid', rs);

            // Return new data
            return HttpResponse.returnSuccessResponse(res, rs)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }
    };

    static async createAuctionByUser(userId: string, activeUntil: Date, code?: string): Promise<any> {
        try {
            // Init model
            const {
                auctions: auctionModel,
                auction_users: auctionUserModel
            } = await global.mysqlDb.repositories(['auctions', 'auction_users'], global.env.defaultDb);

            // Generate code automatically if code is not defined
            if(!code){
                code = helpers.generateVerifyCode(12).toString();
            }


            let insertData = {
                code,
                active_until: activeUntil,
                created_by: userId
            };

            // Insert data to auctions table
            let data =  await auctionModel.save(insertData);

            // Insert data to auction_users table
            await auctionUserModel.save({
                user_id: userId,
                auction_id: data.id,
                bidder_number: `Bidder-Auc${data.id}-${helpers.generateVerifyCode(12).toString()}`
            });

            return data;


        } catch (e) { // Pass error
            throw e
        }
    };

    static async getAuctionList(): Promise<any> {
        try {
            //Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'
            ], global.env.defaultDb);

            return await auctionModel.find({relations: ['created_by']});
        } catch (e) { // Pass error to the next middleware
            throw(e)
        }
    };

    static async startAuction(id: number): Promise<any> {
        try {
            //Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'], global.env.defaultDb);

            return await auctionModel.update(id, {status: 'started'});
        } catch (e) { // Pass error to the next middleware
            throw(e)
        }
    };

    static async pauseAuction(): Promise<any> {
        try {
            //Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'], global.env.defaultDb);

            // Get current running auction
            let auction = await auctionModel.findOne({status: 'started'});

            // No auction has been started
            if(!auction){
                return HttpResponse.returnErrorWithMessage('admin.auction.pause.not.ready')
            }

            // Update auction status
            return await auctionModel.update({id: auction.id}, {status: 'paused'});
        } catch (e) { // Pass error to the next middleware
            throw(e)
        }
    };

    static async resumeAuction(): Promise<any> {
        try {
            //Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'], global.env.defaultDb);

            // Get current running auction
            let auction = await auctionModel.findOne({status: 'paused'});

            // No auction has been started
            if(!auction){
                return HttpResponse.returnErrorWithMessage('admin.auction.resume.not.ready')
            }

            // Update auction status
            return await auctionModel.update(auction.id, {status: 'started'});
        } catch (e) { // Pass error to the next middleware
            throw(e)
        }
    };

    static async stopAuction(): Promise<any> {
        try {
            //Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'], global.env.defaultDb);

            // Get current running auction
            let auction = await auctionModel.findOne({status: 'started'});

            // No auction has been started
            if(!auction){
                return HttpResponse.returnErrorWithMessage('admin.auction.resume.not.ready')
            }

            // Update auction status
            return await auctionModel.update(auction.id, {status: 'stopped'});
        } catch (e) { // Pass error to the next middleware
            throw(e)
        }
    };

    static async sellItem(res: any,hallBid?: boolean, bidderNumber?: number): Promise<any> {
        try {
            //Init model
            const {
                auctions: auctionModel,
                articles: articleModel,
                bids: bidModel,
                auction_users: auctionUserModel,
            } = await global.mysqlDb.repositories(['auctions', 'articles', 'bids', 'auction_users'], global.env.defaultDb);

            // Get current running auction
            let auction = await auctionModel.findOne({status: 'started'});

            // No auction has been started
            if(!auction){
                return HttpResponse.returnBadRequestResponse(res,'admin.auction.not.ready')
            }

            // Get current selling article
            let soldArticle = await articleModel.findOne({auction_id: auction.id, status: '2'});

            // Use hall bid
            if(hallBid){
                // Find this bidder in this auction
                let bidder = await auctionUserModel.findOne({auction_id: auction.id, bidder_number: bidderNumber})

                if(!bidder){
                    return HttpResponse.returnBadRequestResponse(res,'admin.auction.bidder.number.invalid')
                }

                // Find the price this bidder bid
                let bidData = await bidModel.findOne({auction_users_id: bidder.id, articles_id: soldArticle.id});

                // Make status as hall
                await bidModel.update({auction_users_id: bidder.id, articles_id: soldArticle.id},{type: 'hall'});

                if(!bidData){
                    return HttpResponse.returnBadRequestResponse(res,'admin.auction.bidder.number.invalid')
                }

                // Mark this article sold
                await articleModel.update({id: soldArticle.id}, {status: '0', sold_price: bidData.bid});

                // Return the next item
                let sellingArticle =  await articleModel.findOne({auction_id: auction.id, status: '1', id: MoreThan(soldArticle.id)});

                // Still have next item to sell
                if(sellingArticle){
                    // Mark item ready to save
                    await articleModel.update({id: sellingArticle.id}, {status: '2'});
                }

                return sellingArticle


            }else{
                // Find the max bid
                let maxBid = await bidModel
                    .createQueryBuilder("bids")
                    .select('*')
                    .addSelect("max(bids.bid)", "value")
                    .where("bids.articles_id = :id AND bids.type != 'expect'", { id: soldArticle.id })
                    .groupBy('bids.id')
                    .orderBy('MAX(bids.bid)', 'DESC')
                    .getRawOne();

                // There is no bid on this item, still go to the next item, marked as skipped
                if(!maxBid){
                    // Mark this article is down from auction (still available)
                    await articleModel.update({id: soldArticle.id}, {status: '3'});

                    // Mark the next item ready to be on auction
                    let sellingArticle = await articleModel.findOne({auction_id: auction.id, status: '1', id: MoreThan(soldArticle.id)});

                    // Still have next item to sell
                    if(sellingArticle){
                        // Mark item ready to save
                        await articleModel.update({id: sellingArticle.id}, {status: '2'});
                    }

                    return sellingArticle

                }else{
                    // Mark this article sold
                    await articleModel.update({id: soldArticle.id}, {status: '0', sold_price: maxBid.value});

                    // Return the next item
                    let sellingArticle =  await articleModel.findOne({auction_id: auction.id, status: '1', id: MoreThan(soldArticle.id)});

                    // Still have next item to sell
                    if(sellingArticle){
                        // Mark item ready to save
                        await articleModel.update({id: sellingArticle.id}, {status: '2'});
                    }

                    return sellingArticle
                }
            }
        } catch (e) { // Pass error to the next middleware
            throw(e)
        }
    };

    static async undoBid(): Promise<any>{
        //Init model
        const {
            auctions: auctionModel,
            articles: articleModel,
            bids: bidModel,
            auction_users: auctionUserModel,
        } = await global.mysqlDb.repositories(['auctions', 'articles', 'bids', 'auction_users'], global.env.defaultDb);

        // Get current selling article
        let currentArticle =  await articleModel.findOne({status: '2'});

        // Get recent bid
        let recentBid = await bidModel
            .createQueryBuilder("bids")
            .select('*')
            .where("bids.articles_id = :id", { id: currentArticle.id })
            .orderBy('bids.id', 'DESC')
            .getRawOne();

        if(recentBid){
            await bidModel.remove({id: recentBid.id})
        }

        return recentBid
    }

    // Ask bid value
    static async askBid(res, value): Promise<any>{
        //Init model
        const {
            auctions: auctionModel,
            articles: articleModel,
            bids: bidModel,
            auction_users: auctionUserModel,
        } = await global.mysqlDb.repositories(['auctions', 'articles', 'bids', 'auction_users'], global.env.defaultDb);

        // Validate value
        if (typeof parseFloat(value) !== 'number'){
            return HttpResponse.returnBadRequestResponse(res, 'admin.auction.ask.bid.number.invalid')
        }

        // Get current selling article
        let currentArticle =  await articleModel.findOne({status: '2'});

        // Find the max bid
        let maxBid = await bidModel
            .createQueryBuilder("bids")
            .select('*')
            .addSelect("max(bids.bid)", "value")
            .where("bids.articles_id = :id AND bids.type != 'expect'", { id: currentArticle.id })
            .groupBy('bids.id')
            .orderBy('MAX(bids.bid)', 'DESC')
            .getRawOne();

        // Add expect value
        await bidModel.save({
            bid: parseFloat(value),
            type: 'expect',
            articles_id: currentArticle.id
        })

        return maxBid
    }



    static async createNewBid(data: any): Promise<any> {
        try {
            //Init model
            const {
                auctions: auctionModel,
                auction_users: auctionUserModel,
                bids: bidModel
            } = await global.mysqlDb.repositories(['auctions', 'articles', 'bids', 'auction_users'], global.env.defaultDb);

            // Get current running auction
            let auction = auctionModel.findOne({id: data.auction_id, status: 'started'});

            // No auction has been started
            if(!auction){
                return HttpResponse.returnErrorWithMessage('admin.auction.not.ready')
            }

            // Save auction user table
            let rs =  await auctionUserModel.save({
                user_id: data.userId,
                auction_id: data.auction_id,
                bidder_number: `${helpers.generateVerifyCode(5).toString()}`
            });

            // Save bid
            return await bidModel.save({
                auction_users_id: rs.id,
                articles_id: data.articles_id,
                bid: data.bid,
                type: 'live'
            });



        } catch (e) { // Pass error to the next middleware
            throw(e)
        }
    };





    static async getAuctionDetail(id): Promise<any>{
        try{
            // Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'
            ], global.env.defaultDb);

            return await auctionModel.findOne({id});

        }catch (e) { // Pass error to the next middleware
            throw(e)
        }

    };

    static async deleteAuction(id): Promise<any>{
        try{
            // Init model
            const {
                auctions: auctionModel
            } = await global.mysqlDb.repositories(['auctions'], global.env.defaultDb);

            await auctionModel.delete(id);

            return 'Success'

        }catch (e) { // Pass error to the next middleware
            throw(e)
        }

    };


    static async addBidStepToAuction(auctionId: number, data: any): Promise<any> {
        try {
            // Init model
            const {
                bid_steps: bidStepModel
            } = await global.mysqlDb.repositories(['bid_steps'], global.env.defaultDb);


            let insertData = {
                limit: data.limit,
                bid_step: data.bid_step,
                auction_id: auctionId
            };

            // Get resource data
            return await bidStepModel.save(insertData);

        } catch (e) { // Pass error
            throw e
        }
    };


    static async getBidStepList(id: number): Promise<any> {
        try {
            //Init model
            const {
                bid_steps: bidStepModel
            } = await global.mysqlDb.repositories(['bid_steps'], global.env.defaultDb);

            return await bidStepModel.find({auction_id: id});
        } catch (e) { // Pass error to the next middleware
            throw(e)
        }
    };

    static async deleteBidStep(bidStepId: number): Promise<any>{
        try{
            //Init model
            const {
                bid_steps: bidStepModel
            } = await global.mysqlDb.repositories(['bid_steps'
            ], global.env.defaultDb);

            await bidStepModel.delete(bidStepId);

            return 'Success'

        }catch (e) { // Pass error to the next middleware
            throw(e)
        }

    };

    static async createArticle(auctionId: number, data: any): Promise<any>{
        try{
            //Init model
            const {
                articles: articleModel
            } = await global.mysqlDb.repositories(['articles'], global.env.defaultDb);

            data.auction_id = auctionId;

            return await articleModel.save(data);

        }catch (e) { // Pass error to the next middleware
            throw(e)
        }
    };

    static async getArticleList(auctionId: number): Promise<any>{
        try{
            //Init model
            const {
                articles: articleModel
            } = await global.mysqlDb.repositories(['articles'], global.env.defaultDb);

            return await articleModel.find({auction_id: auctionId});

        }catch (e) { // Pass error to the next middleware
            throw(e)
        }

    };

    static async getArticleDetail(articleId: number): Promise<any>{
        try{
            //Init model
            const {
                articles: articleModel
            } = await global.mysqlDb.repositories(['articles'], global.env.defaultDb);

            return await articleModel.findOne({id: articleId});

        }catch (e) { // Pass error to the next middleware
            throw(e)
        }

    };

    static async updateArticle(articleId: number, data): Promise<any>{
        try{
            //Init model
            const {
                articles: articleModel
            } = await global.mysqlDb.repositories(['articles'], global.env.defaultDb);

            return await articleModel.update(articleId, data);

        }catch (e) { // Pass error to the next middleware
            throw(e)
        }

    };

    static async deleteArticle(articleId: number): Promise<any>{
        try{
            //Init model
            const {
                articles: articleModel
            } = await global.mysqlDb.repositories(['articles'], global.env.defaultDb);

            return await articleModel.delete(articleId);

        }catch (e) { // Pass error to the next middleware
            throw(e)
        }

    };

}

