import { Request, Response, NextFunction } from 'express';
import * as passport from 'passport';
import * as request from 'request';
import * as moment from 'moment';
import * as lodash from 'lodash'

import { jwtHelper, helpers } from '../utils';

import ImageService from '../services/image';
import NodeMailerService from '../services/email'
import HttpResponse from '../services/response';
import i18nService from '../services/i18n';

export default class AuthController {
    /**
     * @swagger
     * tags:
     *   - name: Auth
     *     description: Authentication
     */

    /*///////////////////////////////////////////////////////////////
    /////                    START LOCAL LOGIN                 /////
    ///////////////////////////////////////////////////////////////*/

    // Local login

    /**
     * @swagger
     * definitions:
     *   SignIn:
     *     required:
     *       - email
     *       - password
     *       - playerId
     *     properties:
     *       email:
     *         type: string
     *       password:
     *         type: string
     *       playerId:
     *         type: string
     */

    /**
     * @swagger
     * /v1/auth/signIn:
     *   post:
     *     description: Sign in API
     *     tags: [Auth]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/SignIn'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     */

    static async signIn(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Init models
            let {
                user: userModel
            } = await global.mysqlDb.repositories(['user'], global.env.defaultDb);

            // Local authentication
            passport.authenticate('local', {
                session: false
            }, async (err, rs) => {
                // Error, pass to the next middleware
                if (err) {
                    return HttpResponse.returnUnAuthorizeResponse(res, err.errors);
                }

                // Update login
                await userModel.update(rs.data.id, {last_login: new Date()});

                // Get user profile
                let userProfile = await userModel.findOne(rs.data.id)

                // Generate jwt token
                userProfile.token = jwtHelper.signToken(rs.data.id, 'local');

                return HttpResponse.returnSuccessResponse(res, userProfile);

            })(req, res, next);
        } catch (e) {
            next(e)
        }

    };

    /**
     * @swagger
     * definitions:
     *   RequestSignup:
     *     required:
     *       - email
     *     properties:
     *       email:
     *         type: string
     */

    /**
     * @swagger
     * /v1/auth/requestSignUpByEmail:
     *   post:
     *     description: Request signup
     *     tags: [Auth]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/RequestSignup'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     */


    // Request sign up
    static async requestSignUpByEmail(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Init models
            let {
                user: userModel,
                codes: codeModel
            } = await global.mysqlDb.repositories(['user', 'codes'], global.env.defaultDb);

            const { body: data } = req;

            // Check invalid email
            const emailRegex = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if(!emailRegex.exec(data.email)){
                return HttpResponse.returnBadRequestResponse(res, 'email.invalid')
            }

            // Check if whether user existed already
            const existingUser = await userModel.findOne({ email: data.email });

            // Return error
            if (existingUser) {
                if (existingUser.email === data.email) {
                    return HttpResponse.returnDuplicateResponse(res, 'email.existed')
                }
            }

            let randomCode = helpers.generateVerifyCode(12).toString();

            // Save code and verified data to db
            const codeSavedRs = await codeModel.save({
                code: randomCode,
                type: 'verify',
                expired_at: moment().add(60, 'minutes').toDate(), // This code expires in 5 minutes,
                email_request: data.email
            });

            const emailService = new NodeMailerService();

            // Send email to user
            emailService.sendEmail({
                toEmail: data.email,
                name: 'Register',
                code: randomCode
            });


            return HttpResponse.returnSuccessResponse(res, codeSavedRs)

        } catch (e) {
            next(e)
        }
    };

    /**
     * @swagger
     * /v1/auth/signUp:
     *   post:
     *     description: Sign up API (by email)
     *     tags: [Auth]
     *     produces:
     *       - application/json
     *       - multipart/form-data
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - name: file
     *         in: formData
     *         description: Upload avatar
     *         paramType: formData
     *         type: file
     *       - name: first_name
     *         in: formData
     *         description: First name
     *         paramType: formData
     *         type: string
     *       - name: last_name
     *         in: formData
     *         description: Last name
     *         paramType: formData
     *         type: string
     *       - name: email
     *         in: formData
     *         description: Email
     *         paramType: formData
     *         type: string
     *       - name: password
     *         in: formData
     *         description: Password
     *         paramType: formData
     *         type: string
     *       - name: confirmPassword
     *         in: formData
     *         description: Confirm password
     *         paramType: formData
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     */

    // Sign up
    static async signUp(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Init models
            let {
                user: userModel,
                codes: codeModel
            } = await global.mysqlDb.repositories(['user','codes'], global.env.defaultDb);

            // Init image service
            const imageService = new ImageService('local');

            // Process form data
            await imageService.processFormData(req, res);

            const { body: data } = req;

            // Check invalid email
            const emailRegex = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if(!emailRegex.exec(data.email)){
                return HttpResponse.returnBadRequestResponse(res, 'email.invalid')
            }

            // Check if password and confirm password is matched
            if (data.password !== data.confirmPassword) {
                return HttpResponse.returnBadRequestResponse(res, 'password.confirmPassword.not.matched');
            }

            // Check if whether user existed already
            const existingUser = await userModel.findOne({email: data.email});

            // Return error
            if (existingUser) {
                if (existingUser.email === data.email) {
                    return HttpResponse.returnDuplicateResponse(res, 'email.existed')
                }
            }

            //Set role for new user
            data.role = 'user';
            data.password = await helpers.generateHashPassword(data.password);
            data.last_login = new Date();

            let initData = lodash.omitBy({
                ...data
            }, lodash.isUndefined);
            let rs = await userModel.save(initData);

            // Remove password field
            delete rs.password;
            delete rs.confirmPassword;

            return HttpResponse.returnSuccessResponse(res, rs)

        } catch (e) {
            next(e)
        }
    };

    /**
     * @swagger
     * definitions:
     *   RequestResetPassword:
     *     required:
     *       - email
     *     properties:
     *       email:
     *         type: string
     *
     */

    /**
     * @swagger
     * /v1/auth/requestResetPassword:
     *   post:
     *     description: Request to reset password
     *     tags: [Auth]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/RequestResetPassword'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     */

    static async requestResetPassword(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Init  models
            let {
                user: userModel,
                codes: codeModel
            } = await global.mysqlDb.repositories(['user','codes'], global.env.defaultDb);

            const emailService = new NodeMailerService();

            const { body: data } = req;

            // Check invalid email
            const emailRegex = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if(!emailRegex.exec(data.email)){
                return HttpResponse.returnBadRequestResponse(res, 'email.invalid')
            }

            // Check if this user existed
            const userData = await userModel.findOne({ email: data.email });

            // User doesnt not exist, return error
            if (!userData) {
                return HttpResponse.returnBadRequestResponse(res, 'resetPassword.email.not.existed');
            }

            // Generate code and save to collection
            const insertCodeData = await codeModel.save({
                code: await helpers.getRandomCode(),
                type: 'resetPassword',
                email_request: data.email,
                expired_at: moment().add(30, 'minutes').toDate() // This code expires in 30 minutes
            });

            // Send email to user
            emailService.sendResetPasswordEmail({
                toEmail: userData.email,
                name: userData.displayName,
                code: insertCodeData.code
            });

            // Response success
            return HttpResponse.returnSuccessResponse(res, null)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }

    }

    /**
     * @swagger
     * definitions:
     *   ResetPassword:
     *     required:
     *       - code
     *       - password
     *       - confirmPassword
     *     properties:
     *       code:
     *         type: string
     *       password:
     *         type: string
     *       confirmPassword:
     *         type: string
     *
     */

    /**
     * @swagger
     * /v1/auth/resetPassword:
     *   post:
     *     description: Reset password
     *     tags: [Auth]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/ResetPassword'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     */

    static async resetPassword(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Init  models
            let {
                user: userModel,
                codes: codeModel
            } = await global.mysqlDb.repositories(['user','codes'], global.env.defaultDb);

            const { body: data } = req;

            // Check the latest code
            const codeData = await codeModel.findOne({type: 'resetPassword', code: data.code, valid: 1});

            // If code does not exist or too old
            if (!codeData) {
                return HttpResponse.returnBadRequestResponse(res, 'resetPassword.code.invalid');
            }

            // If password and confirm password does not match
            if (data.password !== data.confirmPassword) {
                return HttpResponse.returnBadRequestResponse(res, 'resetPassword.password.confirmPassword.not.match');
            }
            // Generate the new hash password
            const hashPassword = await helpers.generateHashPassword(data.password);

            // Update the new one
            await userModel.update({email: codeData.email_request }, { password: hashPassword });

            // Remove old reset codes
            await codeModel.update({ code: data.code, type: 'resetPassword' },{valid: 0});

            return HttpResponse.returnSuccessResponse(res, null);

        } catch (e) { // Pass error to the next middleware
            next(e)
        }
    }
}

