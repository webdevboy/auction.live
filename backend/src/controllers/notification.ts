import { Request, Response, NextFunction } from 'express';
import * as moment from 'moment';
import * as _ from 'underscore';
import * as lodash from 'lodash';

import { jwtHelper, helpers } from '../utils';

import HttpResponse from '../services/response';

export default class NotificationController {
    /**
     * @swagger
     * tags:
     *   - name: Notification
     *     description: Notification
     */

    /**
     * @swagger
     * /v1/user/notification:
     *   get:
     *     description: Get user notification API
     *     tags: [Notification]
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async getNotification(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Init models
            let {
                notifications: notificationModel
            } = await global.mysqlDb.repositories(['notifications'], global.env.defaultDb);

            let notificationList = await notificationModel.find({relations: ['article'],
                where: {
                    'to_user': req['userProfile'].id,
                },
                order: {
                    sent_time: 'DESC'
                },
            });


            return HttpResponse.returnSuccessResponse(res, notificationList)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }

    };

    /**
     * @swagger
     * /v1/user/notification/markRead/{id}:
     *   put:
     *     description: Mark read notification API
     *     tags: [Notification]
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: path
     *         name: id
     *         description: Notification Id
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async markReadNotification(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Init models
            let {
                notifications: notificationModel
            } = await global.mysqlDb.repositories(['notifications'], global.env.defaultDb);

            const { id } = req.params;

            let notificationList = await notificationModel.update({id: id}, {is_read: true});


            return HttpResponse.returnSuccessResponse(res, notificationList)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }

    };
}

