import { Request, Response, NextFunction } from 'express';
import * as moment from 'moment';
import * as _ from 'underscore';
import * as lodash from 'lodash';

import { jwtHelper, helpers } from '../utils';

import HttpResponse from '../services/response';

export default class UserController {
    /**
     * @swagger
     * tags:
     *   - name: User
     *     description: User
     */

    /**
     * @swagger
     * /v1/user/profile:
     *   get:
     *     description: Get user profile API
     *     tags: [User]
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */

    static async getProfile(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            // Init models
            let {
                user: userModel
            } = await global.mysqlDb.repositories(['user'], global.env.defaultDb);

            const userData = await userModel.findOne({ id: req['userId'] });

            // Remove password field
            delete userData.password;

            return HttpResponse.returnSuccessResponse(res, userData)

        } catch (e) { // Pass error to the next middleware
            next(e)
        }

    };

    /**
     * @swagger
     * definitions:
     *   UpdatedProfile:
     *     required:
     *       - email
     *       - old_password
     *       - new_password
     *       - confirmPassword
     *     properties:
     *       email:
     *         type: string
     *         description: First name
     *       old_password:
     *         type: string
     *         description: First name
     *       new_password:
     *         type: string
     *         description: First name
     *       confirm_password:
     *         type: string
     *         description: First name
     *       first_name:
     *         type: string
     *         description: First name
     *       last_name:
     *         type: string
     *         description: Last name
     *       language:
     *         type: string
     *         description: Language
     *       company_name:
     *         type: string
     *         description: Company name
     *       country:
     *         type: string
     *         description: Country
     *       street:
     *         type: string
     *         description: Street
     *       zipcode:
     *         type: string
     *         description: Zipcode
     *       city:
     *         type: string
     *         description: City
     *       telephone1:
     *         type: string
     *         description: Telephone1
     *       telephone2:
     *         type: string
     *         description: Tetephone2
     *
     */


    /**
     * @swagger
     * /v1/user/profile:
     *   put:
     *     description: Update user profile
     *     tags: [User]
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: lang
     *         in: header
     *         required: false
     *         description: Language key
     *         type: string
     *         enum:
     *              - en
     *              - de
     *         default: de
     *       - in: body
     *         name: body
     *         description: Request body
     *         schema:
     *           $ref: '#definitions/UpdatedProfile'
     *           type: object
     *     responses:
     *       200:
     *         description: Success
     *       400:
     *         description: Invalid request params
     *       401:
     *         description: Unauthorized
     *       404:
     *         description: Resource not found
     *     security:
     *          - auth: []
     */


    static async updateUser(req: Request, res: Response, next: NextFunction): Promise<any>{
        try{
            const { body: data } = req;

            // Init user model
            const {
                user: userModel
            } = await global.mysqlDb.repositories(['user'], global.env.defaultDb);

            // Edit email
            if(data.email !== '' && data.new_password !== '' && data.old_password !== '' && data.confirm_password !== ''){
                // Check invalid email
                const emailRegex = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if(!emailRegex.exec(data.email)){
                    return HttpResponse.returnBadRequestResponse(res, 'email.invalid')
                }

                if(data.new_password !== data.confirm_password){
                    return HttpResponse.returnBadRequestResponse(res, 'user.profile.confirmPassword.not.match')
                }


                // Get current user info
                let userInfo = await userModel
                    .createQueryBuilder('users')
                    .addSelect("users.password")
                    .where({id: req['userProfile'].id})
                    .getOne();

                // Check password is valid
                let compareRs = await helpers.compareHashString(data.old_password, userInfo.password);

                // Password is not matched
                if(!compareRs){
                    return HttpResponse.returnBadRequestResponse(res, 'user.profile.password.not.match')
                }

                // Generate the new pass
                data.password = await helpers.generateHashPassword(data.new_password);
            }else{ // Remove email prop, because user does not update this one
                delete data.email
            }


            // Remove un necessary fields
            delete data.new_password;
            delete data.old_password;
            delete data.confirm_password;


            let initData = lodash.omitBy({
                ...data
            }, lodash.isUndefined);

            // Update
            await userModel.update(req['userProfile'].id, initData);

            // Get new info
            let updatedData = await userModel.findOne(req['userProfile'].id);

            return HttpResponse.returnSuccessResponse(res, updatedData);

        }catch (e) { // Pass error to the next middleware
            next(e)
        }

    };
}

