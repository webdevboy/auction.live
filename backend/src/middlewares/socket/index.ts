import { Request, Response, NextFunction } from 'express';
import HttpResponse from '../../services/response';

export default class SocketMiddleware {

    static isConnectedToSocket(req: Request, res: Response, next: NextFunction): void{

        // Check if this account is connected to socket
        if(!global.socket.isConnected(req['userProfile']._id.toString())){
            // Return error directly
            return HttpResponse.returnBadRequestResponse(res, 'socket.not.connected');
        }

        next()
    }
}