import {MigrationInterface, QueryRunner, Table, TableIndex, TableColumn, TableForeignKey } from "typeorm";

export class User1593943259208 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'CREATE TABLE IF NOT EXISTS users' +
            '(id int AUTO_INCREMENT,' +
            'first_name varchar(255),' +
            'last_name varchar(255),' +
            'email varchar(255) NOT NULL,' +
            'password varchar(255) NOT NULL,' +
            'role ENUM (\'admin\', \'user\', \'guest\') NOT NULL,' +
            'last_login DATETIME,' +
            'status ENUM (\'0\',\'1\',\'2\') DEFAULT \'0\',' +
            'doi BOOLEAN DEFAULT false,' +
            'updated_at DATETIME,' +
            'created_at DATETIME,' +
            'PRIMARY KEY (id))'
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
