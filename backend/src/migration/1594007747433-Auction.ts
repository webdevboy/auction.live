import {MigrationInterface, QueryRunner} from "typeorm";

export class Auction1594007747433 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'CREATE TABLE IF NOT EXISTS auctions' +
            '(id int AUTO_INCREMENT,' +
            'code varchar(255),' +
            'is_active TINYINT,' +
            'active_until DATETIME,' +
            'auction_status ENUM (\'active\',\'inactive\'),' +
            'created_by INT,' +
            'updated_at DATETIME,' +
            'created_at DATETIME,' +
            'PRIMARY KEY (id),' +
            'FOREIGN KEY (created_by) REFERENCES users(id))'
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
