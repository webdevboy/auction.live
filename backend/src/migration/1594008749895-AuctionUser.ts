import {MigrationInterface, QueryRunner} from "typeorm";

export class AuctionUser1594008749895 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'CREATE TABLE IF NOT EXISTS auction_users' +
            '(id int AUTO_INCREMENT,' +
            'user_id INT,' +
            'auction_id INT,' +
            'bidder_number VARCHAR(255),'+
            'updated_at DATETIME,' +
            'created_at DATETIME,' +
            'PRIMARY KEY (id),' +
            'FOREIGN KEY (user_id) REFERENCES users(id),' +
            'FOREIGN KEY (auction_id) REFERENCES auctions(id)' +
            ')'
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
