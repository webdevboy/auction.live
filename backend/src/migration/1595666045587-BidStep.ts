import {MigrationInterface, QueryRunner} from "typeorm";

export class BidStep1595666045587 implements MigrationInterface {
    name = 'BidStep1595666045587'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE IF NOT EXISTS `bid_steps` (" +
            "`id` int NOT NULL AUTO_INCREMENT," +
            "`limit` decimal(10,3) NOT NULL," +
            "`bid_step` decimal(10,3) NOT NULL," +
            "`created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)," +
            "`updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)," +
            "`auction_id` int NULL, " +
            "PRIMARY KEY (`id`), " +
            "FOREIGN KEY (auction_id) REFERENCES auctions(id))" +
            "ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {

    }
}
