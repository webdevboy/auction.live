import {MigrationInterface, QueryRunner} from "typeorm";

export class Article1595666050045 implements MigrationInterface {
    name = 'Article1595666050045'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE IF NOT EXISTS `articles` (" +
            "`id` int NOT NULL AUTO_INCREMENT," +
            "`code` varchar(255) NOT NULL," +
            "`name` varchar(255) NOT NULL," +
            "`description` text NOT NULL," +
            "`limit` decimal(10,3) NOT NULL," +
            "`estimated_price` decimal(10,3) NOT NULL," +
            "`sold_price` decimal(10,3) NOT NULL," +
            "`conditional` tinyint NOT NULL DEFAULT 0," +
            "`live_bidder` tinyint NOT NULL," +
            "`status` enum ('0', '1') NOT NULL DEFAULT '1'," +
            "`created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)," +
            "`updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)," +
            "`auction_id` int NULL, " +
            "PRIMARY KEY (`id`), " +
            "FOREIGN KEY (auction_id) REFERENCES auctions(id)) " +
            "ENGINE=InnoDB");

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
