import {MigrationInterface, QueryRunner} from "typeorm";

export class Bid1595666097153 implements MigrationInterface {
    name = 'Bid1595666097153'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE IF NOT EXISTS `bids` (" +
            "`id` int NOT NULL AUTO_INCREMENT, " +
            "`bid` decimal(10,3) NOT NULL, " +
            "`type` enum ('live', 'hall') NOT NULL, " +
            "`created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), " +
            "`updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), " +
            "`auction_users_id` int NULL, " +
            "`articles_id` int NULL, " +
            "PRIMARY KEY (`id`)," +
            "FOREIGN KEY (auction_users_id) REFERENCES auction_users(id)) " +
            "ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
