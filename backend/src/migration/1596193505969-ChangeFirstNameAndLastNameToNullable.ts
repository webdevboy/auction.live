import {MigrationInterface, QueryRunner} from "typeorm";

export class ChangeFirstNameAndLastNameToNullable1596193505969 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE users MODIFY first_name varchar(255) NULL, MODIFY last_name varchar(255) NULL;')
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
