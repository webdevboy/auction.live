import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCompanyNameCountryStreetZipcodeCityTelephone1Telephone2ToUserTable1596341644765 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        if(!await queryRunner.hasColumn('users', 'language')){
            await queryRunner.query('ALTER TABLE `users` ' +
                'ADD COLUMN language varchar(255) NULL'
            )
        }

        if(!await queryRunner.hasColumn('users', 'company_name')){
            await queryRunner.query('ALTER TABLE `users` ' +
                'ADD COLUMN company_name varchar(255) NULL'
            )
        }

        if(!await queryRunner.hasColumn('users', 'country')){
            await queryRunner.query('ALTER TABLE `users` ' +
                'ADD COLUMN country varchar(255) NULL'
            )
        }

        if(!await queryRunner.hasColumn('users', 'street')){
            await queryRunner.query('ALTER TABLE `users` ' +
                'ADD COLUMN street VARCHAR(255) NULL'
            )
        }

        if(!await queryRunner.hasColumn('users', 'zipcode')){
            await queryRunner.query('ALTER TABLE `users` ' +
                'ADD COLUMN zipcode VARCHAR(10) NULL'
            )
        }

        if(!await queryRunner.hasColumn('users', 'city')){
            await queryRunner.query('ALTER TABLE `users` ' +
                'ADD COLUMN city VARCHAR(255) NULL'
            )
        }

        if(!await queryRunner.hasColumn('users', 'telephone1')){
            await queryRunner.query('ALTER TABLE `users` ' +
                'ADD COLUMN telephone1 VARCHAR(50) NULL'
            )
        }

        if(!await queryRunner.hasColumn('users', 'telephone2')){
            await queryRunner.query('ALTER TABLE `users` ' +
                'ADD COLUMN telephone2 VARCHAR(50) NULL'
            )
        }



        // await queryRunner.query('ALTER TABLE `users` ' +
        //     'ADD COLUMN language varchar(255) NULL AFTER role,' +
        //     'ADD COLUMN company_name varchar(255) NULL AFTER language,' +
        //     'ADD COLUMN country varchar(255) NULL AFTER company_name,' +
        //     'ADD COLUMN street VARCHAR(255) NULL AFTER country,'+
        //     'ADD COLUMN zipcode VARCHAR(10) NULL AFTER street,'+
        //     'ADD COLUMN city VARCHAR(255) NULL AFTER zipcode,'+
        //     'ADD COLUMN tetephone1 VARCHAR(50) NULL AFTER city,'+
        //     'ADD COLUMN tetephone2 VARCHAR(50) NULL AFTER tetephone1'
        // )

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
