import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateStatusOfArticleTableTo012Enum1596358535787 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE articles MODIFY status ENUM (\'0\',\'1\',\'2\') NOT NULL DEFAULT 1;')
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
