import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDefaultValueLiveBidderForArticle1596511429851 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE articles MODIFY live_bidder tinyint NOT NULL DEFAULT 1;')
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
