import {MigrationInterface, QueryRunner} from "typeorm";

export class RenameAuctionStatusToStatusInAuctionTable1596545757301 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        if(await queryRunner.hasColumn('auctions', 'auction_status')){
            await queryRunner.query('ALTER TABLE auctions CHANGE auction_status status ENUM(\'preparing\',\'started\',\'stopped\',\'paused\')')
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
