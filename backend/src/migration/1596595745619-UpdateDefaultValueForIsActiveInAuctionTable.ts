import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateDefaultValueForIsActiveInAuctionTable1596595745619 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE auctions MODIFY is_active tinyint NOT NULL DEFAULT 0;')
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
