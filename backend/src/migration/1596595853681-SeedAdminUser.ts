import {MigrationInterface, QueryRunner} from "typeorm";
import {helpers} from "../utils";
import {UserSeed} from "../seeder/user";

export class SeedAdminUser1596595853681 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        // Hash password
        UserSeed[0].password = await helpers.generateHashString(UserSeed[0].password);



        let user = await queryRunner.query('SELECT * FROM users WHERE email=?',[UserSeed[0].email])

        if(user.length === 0){
            await queryRunner.query('INSERT INTO users (first_name,last_name,company_name,country,street,zipcode,city,telephone1,telephone2,email,password,role,status,last_login) ' +
                'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                [UserSeed[0].first_name,UserSeed[0].last_name,UserSeed[0].company_name,UserSeed[0].country,UserSeed[0].street,UserSeed[0].zipcode,UserSeed[0].city,UserSeed[0].telephone1,UserSeed[0].telephone2,UserSeed[0].email,UserSeed[0].password,UserSeed[0].role,UserSeed[0].status, UserSeed[0].last_login ])
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
