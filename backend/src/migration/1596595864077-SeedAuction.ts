import {MigrationInterface, QueryRunner} from "typeorm";
import {AuctionSeed} from "../seeder/auction";

export class SeedAuction1596595864077 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        let user = await queryRunner.query('SELECT * FROM users WHERE role=?',['admin']);

        await queryRunner.query('INSERT INTO auctions (code,active_until,created_by,status) ' +
            'VALUES (?,?,?,?)',
            [AuctionSeed[0].code, AuctionSeed[0].active_until, user[0].id, AuctionSeed[0].status])
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
