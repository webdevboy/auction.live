import {MigrationInterface, QueryRunner} from "typeorm";
import {ArticleSeed} from "../seeder/article";

export class SeedArticle1596595873512 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        let auction = await queryRunner.query('SELECT * FROM auctions WHERE status=?',['started']);
        console.log(auction[0].id)

        ArticleSeed.map(async(item)=>{
            await queryRunner.query('INSERT INTO articles (code,name,description,`limit`,estimated_price,status,auction_id,sold_price) ' +
                'VALUES (?,?,?,?,?,?,?,?)',
                [item.code,item.name,item.description,item.limit,item.estimated_price,item.status, auction[0].id, item.sold_price])
        })
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
