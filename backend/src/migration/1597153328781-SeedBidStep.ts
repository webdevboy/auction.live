import { MigrationInterface, QueryRunner } from "typeorm";
import { BidStepSeed } from "../seeder/bidStep";

export class SeedBidStep1597153328781 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let auction = await queryRunner.query('SELECT * FROM auctions WHERE status=?',['started']);
        console.log(auction[0].id)

        BidStepSeed.map(async(item)=>{
            await queryRunner.query('INSERT INTO bid_steps (auction_id,bid_step,`limit`) ' +
                'VALUES (?,?,?)',
                [auction[0].id, item.bid_step, item.limit])
        })
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
