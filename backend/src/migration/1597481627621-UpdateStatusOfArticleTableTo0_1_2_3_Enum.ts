import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateStatusOfArticleTableTo0123Enum1597481627621 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE articles MODIFY status ENUM (\'0\',\'1\',\'2\',\'3\') NOT NULL DEFAULT 1;')
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
