import {MigrationInterface, QueryRunner} from "typeorm";

export class AddStatusToBidTable1597499007281 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        if(!await queryRunner.hasColumn('bids', 'status')){
            await queryRunner.query('ALTER TABLE `bids` ' +
                'ADD COLUMN status enum (\'0\', \'1\') NOT NULL DEFAULT \'1\''
            )
        }

        await queryRunner.query('ALTER TABLE bids MODIFY type enum (\'live\', \'hall\', \'expect\') NOT NULL;')
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
