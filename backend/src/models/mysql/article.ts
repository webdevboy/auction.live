import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    getConnection,
    getRepository,
    ManyToOne,
    OneToMany,
    JoinColumn,
    UpdateDateColumn
} from 'typeorm';
import { Auction } from './auction';
import {Notification} from "./notification";

enum Status{
    sold = '0',
    available = '1',
    on_auction = '2', // on going
    skip = '3'
}


@Entity('articles')
export class Article {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column()
    name: string;

    @Column({type: 'text'})
    description: string;

    @Column({type: 'decimal', precision: 10, scale: 3})
    limit: number;

    @Column({type: 'decimal', precision: 10, scale: 3})
    estimated_price: number;

    @Column({type: 'decimal', precision: 10, scale: 3})
    sold_price: number;

    @ManyToOne(type => Auction, auction => auction.id)
    @JoinColumn({ name: "auction_id" })
    auction_id: Auction;

    @Column({type: 'tinyint', default: 0})
    conditional: number;

    @Column({type: 'boolean', default: true})
    live_bidder: boolean;

    @Column("enum", {enum: Status, nullable: false, default: Status.available})
    status: Status;

    @OneToMany(type => Notification, notification => notification.article)
    relatedNotifications: Notification[];

    @CreateDateColumn({type: "timestamp"})
    created_at: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated_at: Date;
}


export const ArticleModel = async (tenantCode) => {
    return getConnection(await global.mysqlInstance.createOrReuseConnection(tenantCode)).getRepository(Article);
};
