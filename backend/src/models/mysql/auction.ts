import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    getConnection,
    getRepository,
    ManyToOne,
    OneToMany,
    JoinColumn,
    UpdateDateColumn
} from 'typeorm';
import { User } from './user';
import { AuctionUser } from "./auctionUser";

enum Status{
    preparing = 'preparing',
    started = 'started',
    stopped = 'stopped',
    paused = 'paused'
}


@Entity('auctions')
export class Auction {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column({type: 'tinyint', default: 0})
    is_active: number;

    @Column({type: 'datetime'})
    active_until: Date;

    @Column("enum", {enum: Status, default: Status.preparing})
    status: Status;

    @ManyToOne(type => User, user => user.auctions)
    @JoinColumn({ name: "created_by" })
    created_by: User;

    @OneToMany(type => AuctionUser, auction => auction.auction_id)
    userToAuction: AuctionUser[];

    @CreateDateColumn({type: "timestamp"})
    created_at: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated_at: Date;
}


export const AuctionModel = async (tenantCode) => {
    return getConnection(await global.mysqlInstance.createOrReuseConnection(tenantCode)).getRepository(Auction);
};
