import "reflect-metadata";
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    getConnection,
    ManyToOne,
    JoinColumn,
    UpdateDateColumn
} from 'typeorm';
import { User } from './user';
import { Auction} from './auction';

@Entity('auction_users')
export class AuctionUser {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, user => user.userToAuction)
    @JoinColumn({ name: "user_id" })
    user_id!: User;

    @ManyToOne(type => Auction, auction => auction.userToAuction)
    @JoinColumn({ name: "auction_id" })
    auction_id!: Auction;

    @Column()
    bidder_number: string;

    @CreateDateColumn({type: "timestamp"})
    created_at: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated_at: Date;
}


export const AuctionUserModel = async (tenantCode) => {
    return getConnection(await global.mysqlInstance.createOrReuseConnection(tenantCode)).getRepository(AuctionUser);
};
