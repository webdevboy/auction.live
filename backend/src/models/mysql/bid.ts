import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    getConnection,
    getRepository,
    ManyToOne,
    OneToMany,
    JoinColumn,
    UpdateDateColumn
} from 'typeorm';
import { Article } from './article';
import { AuctionUser } from './auctionUser';

enum Type{
    live = 'live',
    hall = 'hall',
}

enum Status{
    active = '1',
    inactive = '0'
}


@Entity('bids')
export class Bid {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => AuctionUser, auctionUser => auctionUser.id)
    @JoinColumn({ name: "auction_users_id" })
    auction_users_id: AuctionUser;

    @ManyToOne(type => Article, article => article.id)
    @JoinColumn({ name: "articles_id" })
    articles_id: AuctionUser;

    @Column({type: 'decimal', precision: 10, scale: 3})
    bid: number;

    @Column("enum", {enum: Type, nullable: false})
    type: Type;

    @Column("enum", {enum: Status, default: Status.active})
    status: Status;

    @CreateDateColumn({type: "timestamp"})
    created_at: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated_at: Date;
}


export const BidModel = async (tenantCode) => {
    return getConnection(await global.mysqlInstance.createOrReuseConnection(tenantCode)).getRepository(Bid);
};
