import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    getConnection,
    getRepository,
    ManyToOne,
    OneToMany,
    JoinColumn,
    UpdateDateColumn
} from 'typeorm';
import { Auction } from './auction';

enum Status{
    active = 'active',
    inactive = 'inactive'
}


@Entity('bid_steps')
export class BidStep {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'decimal', precision: 10, scale: 3})
    limit: number;

    @Column({type: 'decimal', precision: 10, scale: 3})
    bid_step: number;

    @ManyToOne(type => Auction, auction => auction.id)
    @JoinColumn({ name: "auction_id" })
    auction_id: Auction;

    @CreateDateColumn({type: "timestamp"})
    created_at: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated_at: Date;
}


export const BidStepModel = async (tenantCode) => {
    return getConnection(await global.mysqlInstance.createOrReuseConnection(tenantCode)).getRepository(BidStep);
};
