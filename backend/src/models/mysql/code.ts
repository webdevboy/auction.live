import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    getConnection,
    getRepository,
    ManyToOne,
    OneToMany,
    JoinColumn,
    UpdateDateColumn
} from 'typeorm';

@Entity('codes')
export class Code {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column()
    type: string;

    @Column()
    email_request: string;

    @Column({type: 'datetime'})
    expired_at: Date;

    @Column({type: 'boolean', default: true})
    valid: boolean;

    @CreateDateColumn({type: "timestamp"})
    created_at: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated_at: Date;
}


export const CodeModel = async (tenantCode) => {
    return getConnection(await global.mysqlInstance.createOrReuseConnection(tenantCode)).getRepository(Code);
};
