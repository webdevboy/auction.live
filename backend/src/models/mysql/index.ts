import {User, UserModel} from './user';
import {Auction, AuctionModel} from './auction';
import {AuctionUser, AuctionUserModel} from './auctionUser';
import {BidStep, BidStepModel} from './bidStep';
import {Article, ArticleModel} from './article';
import {Bid, BidModel} from './bid';
import {Code, CodeModel} from './code';
import {Notification, NotificationModel} from './notification';

import "reflect-metadata";

const models = [User, Auction, AuctionUser, BidStep, Article, Bid, Code, Notification];

export async function repositories(tables: string[], tenantCode: string) {
    let connections = global.mysqlDb.connections;

    // Get repositories
    for (let i = 0; i < tables.length; i++) {

        if (connections[tables[i]]) {
            continue;
        }

        switch (tables[i]) {
            case 'user':
                connections[tables[i]] = await UserModel(tenantCode);
                break;

            case 'auctions':
                connections[tables[i]] = await AuctionModel(tenantCode);
                break;

            case 'auction_users':
                connections[tables[i]] = await AuctionUserModel(tenantCode);
                break;

            case 'bid_steps':
                connections[tables[i]] = await BidStepModel(tenantCode);
                break;

            case 'articles':
                connections[tables[i]] = await ArticleModel(tenantCode);
                break;

            case 'bids':
                connections[tables[i]] = await BidModel(tenantCode);
                break;

            case 'codes':
                connections[tables[i]] = await CodeModel(tenantCode);
                break;

            case 'notifications':
                connections[tables[i]] = await NotificationModel(tenantCode);
                break;
        }
    }

    global.mysqlDb.connections = {...global.mysqlDb.connections, ...connections};

    return connections;
}

export default models;
