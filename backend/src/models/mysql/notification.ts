import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    getConnection,
    getRepository,
    ManyToOne,
    OneToMany,
    JoinColumn,
    UpdateDateColumn,
    OneToOne
} from 'typeorm';

import { User } from './user';
import { Article } from './article';

@Entity('notifications')
export class Notification {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Article, article => article.relatedNotifications)
    @JoinColumn({ name: "article" })
    article: Article;

    @ManyToOne(type => User, user => user.notifications)
    @JoinColumn({ name: "to_user" })
    to_user: User;

    @Column({nullable: true})
    message: string;

    @Column({type: "datetime"})
    sent_time: Date;

    @Column({type: 'boolean', default: false})
    is_read: boolean;

    @CreateDateColumn({type: "timestamp"})
    created_at: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated_at: Date;
}


export const NotificationModel = async (tenantCode) => {
    return getConnection(await global.mysqlInstance.createOrReuseConnection(tenantCode)).getRepository(Notification);
};
