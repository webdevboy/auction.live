import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, getConnection, getRepository, OneToMany,  } from 'typeorm';
import { Auction } from './auction';
import { AuctionUser } from './auctionUser';
import { Notification } from './notification';

enum Role{
    admin = 'admin',
    user = 'user',
    guest = 'guest'
}

enum Status{
    pending = '0',
    locked = '1',
    unlocked = '2',
}

enum Language{
    english = 'english',
    german = 'german',
}


@Entity('users')
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true})
    first_name: string;

    @Column({nullable: true})
    last_name: string;

    @Column({nullable: true})
    company_name: string;

    @Column({nullable: true})
    country: string;

    @Column({nullable: true})
    street: string;

    @Column({nullable: true})
    zipcode: string;

    @Column({nullable: true})
    city: string;

    @Column({nullable: true})
    telephone1: string;

    @Column({nullable: true})
    telephone2: string;

    @Column()
    email: string;

    @Column({select: false })
    password: string;

    @Column("enum", {enum: Role, nullable: false})
    role: Role;

    @Column()
    last_login: Date;

    @Column("enum", {enum: Status, default: Status.unlocked})
    status: Status;

    @Column({default: false})
    doi: boolean;

    @Column('enum',{ default: Language.german, enum: Language})
    language: Language;

    @OneToMany(type => Auction, auction => auction.created_by)
    auctions: Auction[];

    @OneToMany(type => AuctionUser, auction => auction.user_id)
    userToAuction: AuctionUser[];

    @OneToMany(type => Notification, notification => notification.to_user)
    notifications: Notification[];

    @CreateDateColumn({type: "timestamp"})
    created_at: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated_at: Date;
}


export const UserModel = async (tenantCode) => {
    return getConnection(await global.mysqlInstance.createOrReuseConnection(tenantCode)).getRepository(User);
};
