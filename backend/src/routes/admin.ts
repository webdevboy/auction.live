import * as express from 'express';

import AuthMiddleware from '../middlewares/auth';
import AdminController from '../controllers/admin';

const adminRoute = express.Router();

/*///////////////////////////////////////////////////////////////
/////                 START AUTH MIDDLEWARE                 /////
///////////////////////////////////////////////////////////////*/
adminRoute.use(AuthMiddleware.isAuthenticated);
adminRoute.use(AuthMiddleware.isAdmin);

// User APIs

adminRoute.route('/user/list')
    .get(AdminController.getUserList)

adminRoute.route('/user/:userId')
    .delete(AdminController.deleteUser)
    .put(AdminController.updateUser);

adminRoute.route('/user/activate/:userId')
    .put(AdminController.activateUser);


// Auction APIs

adminRoute.route('/auction')
    .post(AdminController.createAuction);

adminRoute.route('/auction/list')
    .get(AdminController.getAuctionList);

adminRoute.route('/auction/start/:id')
    .put(AdminController.startAuction);

adminRoute.route('/auction/doAction')
    .put(AdminController.doAction);

adminRoute.route('/auction/jumpToArticle/:id')
    .put(AdminController.jumpArticle);

adminRoute.route('/auction/:id')
    .get(AdminController.getAuctionDetail)
    .delete(AdminController.deleteAuction);

// Bid step APIs
adminRoute.route('/auction/:id/bid-step')
    .post(AdminController.addBidStep);

adminRoute.route('/auction/:id/bid-step')
    .get(AdminController.getBidStepList);

adminRoute.route('/auction/bid-step/:id')
    .delete(AdminController.deleteBidStep);


// Article APIs
adminRoute.route('/article')
    .post(AdminController.createArticle);

adminRoute.route('/article/:id')
    .get(AdminController.getArticleDetail)
    .delete(AdminController.deleteArticle);

adminRoute.route('/article/:auctionId/list')
    .get(AdminController.getArticleList);




/*///////////////////////////////////////////////////////////////
/////                  END AUTH MIDDLEWARE                 /////
///////////////////////////////////////////////////////////////*/

export default adminRoute;
