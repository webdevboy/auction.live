import * as express from 'express';

import AuthMiddleware from '../middlewares/auth';
import AuctionController from '../controllers/auction';

const auctionRoute = express.Router();

/*///////////////////////////////////////////////////////////////
/////                 START AUTH MIDDLEWARE                 /////
///////////////////////////////////////////////////////////////*/
auctionRoute.use(AuthMiddleware.isAuthenticated);

auctionRoute.route('/active')
    .get(AuctionController.getCurrentActiveAuction);

auctionRoute.route('/doBid/:auctionId/:articleId')
    .put(AuctionController.doBid);




/*///////////////////////////////////////////////////////////////
/////                  END AUTH MIDDLEWARE                 /////
///////////////////////////////////////////////////////////////*/

export default auctionRoute;
