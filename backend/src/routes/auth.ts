import * as express from 'express';

import AuthController from '../controllers/auth';

const authRoute = express.Router();
// Local login
authRoute.route('/signIn')
    .post(AuthController.signIn);

// Local sign up
authRoute.route('/signUp')
    .post(AuthController.signUp);

authRoute.route('/requestSignUpByEmail')
    .post(AuthController.requestSignUpByEmail);

// Request to reset password
authRoute.route('/requestResetPassword')
    .post(AuthController.requestResetPassword);

// Request to reset password
authRoute.route('/resetPassword')
    .post(AuthController.resetPassword);

export default authRoute;
