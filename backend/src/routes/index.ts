import * as express from 'express';

import authRoute from './auth'
import userRoute from './user';
import auctionRoute from './auction'
import adminRoute from './admin';

const baseApi = express.Router();
const adminApi = express.Router();

// Default public route
export = (app) => {
    // Admin APIs
    adminApi.use('/admin', adminRoute);

    // Auth APIs
    baseApi.use('/auth', authRoute);

    // User APIs
    baseApi.use('/user', userRoute);

    // Auction APIs
    baseApi.use('/auction', auctionRoute);

    app.use('/api/v1', baseApi);
    app.use('/api/v1', adminApi);
};
