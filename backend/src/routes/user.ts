import * as express from 'express';

import AuthMiddleware from '../middlewares/auth';
import UserController from '../controllers/user';
import NotificationController from '../controllers/notification'

const userRoute = express.Router();

/*///////////////////////////////////////////////////////////////
/////                 START AUTH MIDDLEWARE                 /////
///////////////////////////////////////////////////////////////*/
userRoute.use(AuthMiddleware.isAuthenticated);

userRoute.route('/profile')
    .get(UserController.getProfile)
    .put(UserController.updateUser);


userRoute.route('/notification')
    .get(NotificationController.getNotification)

userRoute.route('/notification/markRead/:id')
    .put(NotificationController.markReadNotification)




/*///////////////////////////////////////////////////////////////
/////                  END AUTH MIDDLEWARE                 /////
///////////////////////////////////////////////////////////////*/

export default userRoute;
