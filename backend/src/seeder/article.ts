export const ArticleSeed = [
    {
        code: 'ARC_01',
        name: 'Frederic Remington Large Bronze Sculpture',
        description: 'Lorem ipsum dolor sit amet, dolor nibh et, sapien vitae velit vivamus faucibus. Nec maecenas eget in amet at lacus, nam amet quis porttitor felis per metus, nibh libero elit, ut turpis sit vitae, vivamus vestibulum wisi pharetra. Vitae tempor sed. Ut erat ante cras, natoque consequat sed tellus interdum intege',
        limit: 200,
        estimated_price: 300,
        status: '2',
        auction_id: 1,
        sold_price: 0
    },
    {
        code: 'ARC_02',
        name: 'Old Super Great Bowl',
        description: 'Lorem ipsum dolor sit amet, dolor nibh et, sapien vitae velit vivamus faucibus. Nec maecenas eget in amet at lacus, nam amet quis porttitor felis per metus, nibh libero elit, ut turpis sit vitae, vivamus vestibulum wisi pharetra. Vitae tempor sed. Ut erat ante cras, natoque consequat sed tellus interdum intege',
        limit: 100,
        estimated_price: 200,
        status: '1',
        auction_id: 1,
        sold_price: 0
    }
];
