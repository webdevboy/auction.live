export const UserSeed = [
    {
        first_name: 'Auction Live',
        last_name: 'Admin',
        company_name: '',
        country: '',
        street: '',
        zipcode: '',
        city: '',
        telephone1: '',
        telephone2: '',
        email: 'admin@auctionlive.com',
        password: '12345678',
        role: 'admin',
        status: '2',
        last_login: '2020-01-01 10:10:10'
    }
];
