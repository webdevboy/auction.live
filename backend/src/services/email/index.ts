import { createTransport } from 'nodemailer';
const hbs = require('nodemailer-express-handlebars');

import helpers from '../../utils/helpers';

interface ISendEmail {
    userId?: string,
    toEmail: string,
    name: string,
    code: string
}

export default class NodeMailerService {
    private smtpTransport: any;

    constructor() {
        const { email } = global.configs;

        this.smtpTransport = createTransport({
            service: email.serial,
            host: email.host,
            port: email.port,
            auth: {
                user: email.username,
                pass: email.password
            },
            tls: {
                rejectUnauthorized: false
            },
            debug: true
        });

        this.smtpTransport.use(
            'compile',
            hbs({
                viewEngine: {
                    extName: '.hbs',
                    partialsDir: 'src/views/email/',
                    layoutsDir: 'src/views/email/',
                    defaultLayout: 'requestSignup.hbs',
                },
                viewPath: 'src/views/email/',
                extName: '.hbs'
            })
        );
    }

    public async sendEmail(data: ISendEmail): Promise<any> {
        const { email } = global.configs;
        const { toEmail, name, code } = data;

        const link = `${email.basepath}update-info?code=${code}&email=${encodeURIComponent(toEmail)}`;

        const mailOptions = {
            from: 'Auction Live', // sender address
            to: toEmail, // list of receivers
            subject: 'Auction Live Registration ( Not reply)', // Subject line
            template: 'requestSignup',
            context: {
                name: toEmail,
                email: toEmail,
                url: link,
            }
        };

        return await this.smtpTransport.sendMail(mailOptions);
    }

    public async sendResetPasswordEmail(data: ISendEmail): Promise<any> {
        const { email } = global.configs;
        const { toEmail, name, code } = data;

        const link = `${email.basepath}resetpassword?code=${code}}`;

        const mailOptions = {
            from: 'Auction Live', // sender address
            to: toEmail, // list of receivers
            subject: 'Auction Live Reset Password ( Not reply)', // Subject line
            template: 'resetPassword',
            context: {
                name: toEmail,
                email: toEmail,
                url: link,
            }
        };

        return await this.smtpTransport.sendMail(mailOptions);
    }

    public async sendEmailResetForAdmin(data: ISendEmail): Promise<any> {

        const { toEmail, name, code } = data;
        const webBaseUrl = global.env.webBaseUrl;
        const resetPasswordLink = await helpers.getAdminResetPasswordUrl(webBaseUrl, code);

        const mailOptions = {
            from: 'GeToGeDa', // sender address
            to: toEmail, // list of receivers
            subject: 'GeToGeDa Reset Password ( Not reply)', // Subject line
            template: 'resetPassword',
            context: {
                name,
                email: toEmail,
                url: resetPasswordLink,
            }
        };

        return await this.smtpTransport.sendMail(mailOptions);
    }
}
