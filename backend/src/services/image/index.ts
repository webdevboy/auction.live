import * as multer from 'multer';
import * as path from 'path';
import * as q from 'q';
import * as fs from 'fs';

// Accepted upload image type
const acceptedImageType = 'image/jpg, image/jpeg, image/jps, image/png, image/gif';

export default class ImageService{

    private uploadInstance: any;
    private uploadDestination: string;
    private s3Configs: any;
    private localConfigs: any;
    private s3Instance: any;

    constructor(type: string){

        const { s3, local } = global.configs.uploadImage;

        this.uploadDestination = type;
        this.s3Configs = s3;
        this.localConfigs = local;

        // File filter, only accept specific image files
        const fileFilter = (req, file, cb) => {
            if (acceptedImageType.indexOf(file.mimetype) === -1) {
                cb(new Error('Accept only file types: ' + acceptedImageType));
            } else {
                cb(null, true);
            }
        };

        // Init upload instance
        switch (type){
            case 'local': {
                // Init upload instance
                this.uploadInstance = multer({
                    fileFilter,
                    limits: {
                        fileSize: this.s3Configs.imageUploadSizeLimit * (1024 * 1024), // [Limit size] x 1MB (1*1024*1024)
                    },
                }).single('file');

                break;
            }
        }
    }

    // Process form data from request
    public async processFormData(req: any, res: any): Promise<any>{
        let defer = q.defer();

        // Start to upload images
        this.uploadInstance(req, res, err => {
            if (err) {  // Error
                defer.resolve({
                    error: true,
                    message: err.message
                })
            } else {
                defer.resolve();
            }
        });
        return defer.promise;
    }

    // Upload file
    public async upload(file: any): Promise<any>{
        let defer = q.defer();

        const fileName = `${path.basename(file.originalname).split('.')[0]}-${new Date().getTime()}${path.extname(file.originalname)}`;
        const writeFilePath = `${this.localConfigs.localStoragePath}/${fileName}`;
        const saveFileUrl = `${global.env.basePath}images/${fileName}`;

        // Local Upload
        if(this.uploadDestination === 'local'){
            fs.writeFile(writeFilePath, file.buffer, err => {
                if (err) {
                    defer.resolve({
                        error: true,
                        message: err.message
                    })
                } else {
                    defer.resolve({
                        error: false,
                        imageUrl: saveFileUrl
                    });
                }
            });
        }

        // S3 Upload
        if(this.uploadDestination === 's3'){
            const params = {
                Bucket: this.s3Configs.bucket,
                Key: `Avatar/${fileName}`,
                Body: file.buffer,
                ContentType: file.mimetype,
                Metadata: {fieldname: file.fieldname},
                ACL: 'private'
            };


            this.s3Instance.upload(params, (err, data) => {
                if (err) {
                    defer.resolve({
                        error: true,
                        message: err.message
                    })
                } else {
                    defer.resolve({
                        error: false,
                        imageUrl: data.Location
                    });
                }
            })
        }


        return defer.promise;
    }

}
