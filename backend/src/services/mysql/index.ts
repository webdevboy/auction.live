import { createConnection, Connection, getConnection } from "typeorm";
import Logger from '../logger';
import models from '../../models/mysql';

export default class MySQL {
    private dbConfigs: any;
    private isDisconnected: boolean;
    private logger: Logger;
    private tenantCode: string = null;
    private connections = [];

    constructor(dbConfigs: object, tenantCode?: string) {
        this.isDisconnected = false;
        this.dbConfigs = dbConfigs;
        this.logger = global.logger;
        if (tenantCode) {
            this.tenantCode = tenantCode;
        }
    }

    // Create the connection
    public async createOrReuseConnection(tenantCode: string): Promise<string> {
        try {
            console.log('create or reuse connection')
            let existingConnectionIndex = this.connections.indexOf(tenantCode);

            // Connection existed already
            if (existingConnectionIndex > -1) {

                return tenantCode

            } else { // If not, create the new connections
                if (tenantCode) {

                    this.dbConfigs.name = tenantCode;
                    this.dbConfigs.database = tenantCode;
                    this.dbConfigs.entities = models;
                    this.dbConfigs.bigNumberStrings = false

                }

                // Crate database connection with specific config
                const connection = await createConnection(this.dbConfigs);

                // Automatically generate table if it doesnt exist
                await connection.synchronize();


                if (connection.isConnected) {

                    this.connections.push(this.dbConfigs.name);
                    this.logger.info(`Connected to :  ${this.dbConfigs.host}:${this.dbConfigs.port}`);

                } else {

                    this.logger.info(`Disconnecting to :  ${this.dbConfigs.host}:${this.dbConfigs.port}`);

                }

                return tenantCode
            }
        } catch (err) {

            console.log(err)

            this.logger.error(err.message);

            return;
        }
    }
}
