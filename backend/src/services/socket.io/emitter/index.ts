import * as _ from 'underscore';

import SocketIOHelper from '../helper';
import Logger from '../../logger';
import HttpResponse from '../../response';

const socketIOTitle = '[Socket.io]';

export default class SocketIOEmitter {
    private socketHelper: SocketIOHelper;
    logger: Logger;

    constructor() {
        this.socketHelper = new SocketIOHelper();
        this.logger = new Logger();
    }

    // Emit event to to user Id Arr
    public async emitEventToUserIdArr(userIdArr: string[], eventName: string, data: any): Promise<CustomResponse> {
        // Get connected client list
        const clientList = this.socketHelper.getConnectedClientListByUserIdArr(this.socketHelper.getClientList(), userIdArr);

        // Emit event to them
        return await this.emitToClients(clientList, eventName, data);
    }

    // Emit event to to room Id Arr
    public async emitEventToRoomIdArr(roomArr: string[], eventName: string, data: any): Promise<CustomResponse> {
        // Emit event to them

        return await this.emitToRooms(this.socketHelper.getSocketIOInstance(), roomArr, eventName, data);
    }

    // Emit event to to all connected users
    public async emitEventToAllConnectedUser(eventName: string, data: any): Promise<CustomResponse> {
        // Emit event to them
        return await this.emitToClients(this.socketHelper.getClientList(), eventName, data);
        //const io = this.socketHelper.getSocketIOInstance();

        //return io.emit(eventName, data)
    }

    private async emitToClients(clientList: object[], eventName: string, data: any): Promise<CustomResponse> {
        try {
            // Emit event for each client
            _.each(clientList, client => {
                client.socket.emit(eventName, data);
                //this.logger.debug(`${socketIOTitle} Emit event ${eventName} on user ${client.id}`);
            });

            // Return success
            return HttpResponse.returnSuccess();
        } catch (err) {
            // Return error
            return HttpResponse.returnErrorWithMessage(err.message)
        }
    }

    private async emitToRooms(io: any, roomArr: string[], eventName: string, data: any): Promise<CustomResponse> {
        try {
            // Emit event for each room
            _.each(roomArr, room => {
                io.sockets.in(room).emit(eventName, data)
                //this.logger.debug(`${socketIOTitle} Emit event ${eventName} on room ${room}`);
            });

            // Return success
            return HttpResponse.returnSuccess();
        } catch (err) {
            // Return error
            return HttpResponse.returnErrorWithMessage(err.message)
        }
    }

    // Emit event to to all connected admins
    public async emitEventToAllConnectedAdmin(eventName: string, data: any): Promise<CustomResponse> {
        const io = this.socketHelper.getSocketIOInstance();

        return io.sockets.in("adminRoom").emit(eventName, data);
    }

    // Emit event to to all connected editors
    public async emitEventToAllConnectedEditor(eventName: string, data: any): Promise<CustomResponse> {
        const io = this.socketHelper.getSocketIOInstance();

        return io.sockets.in("editorRoom").emit(eventName, data);
    }
}
