import * as _ from 'underscore';

import HttpResponse from '../../response';
import CustomResponse from '../../response/customResponse';
import Logger from '../../logger';

const socketIOTitle = '[Socket.io]';

export default class SocketIOHelper {
    private logger: Logger;

    constructor(){
        this.logger = new Logger();
    }

    // Add an socket to list
    public async addSocket(socketClientList: any, userInfo: any, socket: any): Promise<CustomResponse>{
        let isUpdate = false;
        let result = new CustomResponse();

        try{
            if (_.isObject(socketClientList) && _.isObject(userInfo) && _.isObject(socket)) {
                // Add this socket to user info
                Object.assign(userInfo, {socket});

                // Check if this socket is already existed
                if(socketClientList[userInfo.id]){
                    // Marked this is update action
                    isUpdate = true;

                    // Replace old socket by the new one at index
                    result = await this.updateSocketById(socketClientList, userInfo.id, userInfo);
                }else {
                    // Store the new socket
                    socketClientList[userInfo.id] = userInfo
                }
            }else{ // Invalid params
                result = HttpResponse.returnErrorWithMessage('Invalid parameters: [userInfo, socket] must be object');
            }

            // All success, write log
            if (!result.error) {
                this.logger.debug(`${socketIOTitle} ${isUpdate ? 'Updated' : 'Added'} socket with id ${socket.id} of user ${userInfo['email'] || userInfo['id']}`);
            }

            return result;

        }catch(err){
           return  HttpResponse.returnErrorWithMessage(err.message)
        }
    };

    // Replace a old socket by the new one by Id
    private async updateSocketById(socketClientList: any, id: number, newSocket): Promise<CustomResponse>{
        socketClientList[id] = newSocket;

        // Return success
        return HttpResponse.returnSuccess();
    };

    // Remove socket by Id
    public removeSocketFromList(socketClientList: any, socket: any): void{
        const { id } = socket.handshake.query.userInfo;
        // If this socket connection is running
        if (socketClientList[id]) {
            // Close this socket
            this.closeSocket(socket);

            // Remove this socket from list
           delete socketClientList[id];

            // Log
            this.logger.debug(`${socketIOTitle} Removed socket with id ${socket.id}`);
        }
    };

    // Close socket connection
    public closeSocket(socket: any): void{
        socket.disconnect(true);
    }

    // Get connected client info by their ids
    public getConnectedClientListByUserIdArr(socketClientList: any, userIdArr: string[]): object[]{
        let clientList = [];

        _.each(userIdArr, function(userId) {
            // Get desired available connect client
            if(userId && socketClientList[userId.toString()]){
                clientList.push(socketClientList[userId]);
            }
        });

        return clientList;
    }

    // Leave specific room by user ids
    public leaveRoomByUserIdArr(userIdArr: string[], roomId: string){
        _.each(userIdArr, userId => {
            if(global.socket.clientList[<string>userId]){
                // Update play in room info
                global.socket.clientList[<string>userId].playInRoom = null;
                // Leave socket room
                global.socket.clientList[<string>userId].socket.leave(roomId, () => {
                    this.logger.debug(`${socketIOTitle} Client ${userId} with socket Id: ${global.socket.clientList[userId].socket.id} has just left room ${roomId}`);
                });
            }

        });
    };

    public getClientList(): object[]{
        return global.socket.clientList;
    }

    public getSocketIOInstance(): any{
        return global.socket.io;
    }
}