// Libraries
import { Server } from 'http';
import * as initSocket from 'socket.io';
import * as _ from 'underscore';
import * as JWT from 'jsonwebtoken';

// Utils
import Logger from '../logger';
import SocketIOHelper from './helper';
import SocketIOEmitter from './emitter';
import HttpResponse from '../response';

import { helpers } from '../../utils';

import AuctionController from '../../controllers/auction';

const socketIOTitle = '[Socket.io]';

export default class SocketIO {
    private logger: Logger;
    private clientList: object = {};
    private socketHelper: SocketIOHelper;
    private socketEmitter: SocketIOEmitter;
    private io: any;

    constructor() {
        this.logger = new Logger();
        this.socketHelper = new SocketIOHelper();
        this.socketEmitter = new SocketIOEmitter();
    }

    public run(server: Server): void {
        // Start socket
        this.io = initSocket(server, {
            pingTimeout: 10000 // How many ms without a pong packet to consider the connection closed
        });

        this.logger.info(`${socketIOTitle} Listening new connection...`);

        // Default namespace for other user except admin
        this.io.of(/^((?!admins).)*$/gm).use((socket, next) => {
            console.log('start handsake')
            if (socket.handshake.query && socket.handshake.query.token) {
                JWT.verify(socket.handshake.query.token, global.env.jwtSecret, async (err, decoded) => {
                    if (err) {
                        // Return more clear message if jwt token is expired
                        if (err.message === 'Jwt is expired') {
                            let expireTime = _.isObject(err.parsedBody) ? new Date(err.parsedBody.exp * 1000) : '';
                            next(new Error('Your token expired at ' + expireTime))
                        } else {  // Other error
                            next(new Error(err))
                        }
                    } else { // Valid token, get user info
                        // Init models
                        let { user: userModel } = await global.mysqlDb.repositories(['user'], global.env.defaultDb);

                        let query = {};

                        // Local account, find by its own Id
                        if (decoded.provider === 'local') {
                            Object.assign(query, {id: decoded.id})
                        } else { // Social account
                            Object.assign(query, {socialId: decoded.id})
                        }

                        // Check if this user existed
                        userModel.findOne(query).then((rs) => {
                            if (rs) {
                                // Check if this account is active
                                if (rs.status === '2') {
                                    // Allow connect
                                    socket.handshake.query.userInfo = rs;
                                    next();
                                } else { // Inactive account
                                    next(new Error('Inactive account'));
                                }
                            } else { // Token doest not belong to this account
                                next(new Error('Authentication error'))
                            }
                        });
                    }
                });
            } else {
                next(new Error('Authentication error'));
            }
        })
        // Client connect listener
            .on('connection', async client => {
                this.logger.debug(`${socketIOTitle} Client connected: SocketID = ${client.id}`);

                // Get language from client
                const lang = client.handshake.query.lang;

                // Add to socket client list
                const rs = await this.socketHelper.addSocket(this.clientList, client.handshake.query.userInfo, client);

                // Close this socket due to error
                if (rs.error) {
                    client.disconnect();
                    this.logger.debug(`${socketIOTitle} Disconnect client with SocketID = ${client.id} due to ${rs.errors} `);
                }

                // Client disconnect listener
                client.on('disconnect', async data => {
                    // Remove socket from list
                    let id = this.socketHelper.removeSocketFromList(this.clientList, client);


                    this.logger.debug(`Remove user ${id}`);

                    this.logger.debug(`${socketIOTitle} Client disconnected: SocketID = ${client.id} `);

                });

                // Event listener for auction handler (for user)
                client.on('bid', async (receiveData, cb) => {
                    let { type, data } = receiveData;
                    try {
                        switch (type){
                            case 'new': {
                                try {
                                    let res = await AuctionController.createNewBid(data);

                                    // Send for all user about this update
                                    this.sendNewEventToAllConnectedUser('updateBid', res);

                                    cb(res);
                                } catch (e) {
                                    global.logger.debug(`User ${client.handshake.query.userInfo._id} emit event ${type}`);
                                    global.logger.debug(e);

                                    cb(e);
                                }
                                break;
                            }
                        }
                    } catch (e) {
                        global.logger.debug(`Player ${client.handshake.query.userInfo._id} emit event ${type}`);
                        global.logger.debug(e);

                        cb(e);
                    }
                });
            });


        // Admin namespace
        this.io.of('/admins').use((socket, next) => {
            if (socket.handshake.query && socket.handshake.query.token) {
                JWT.verify(socket.handshake.query.token, global.env.jwtSecret, async (err, decoded) => {
                    if (err) {
                        // Return more clear message if jwt token is expired
                        if (err.message === 'Jwt is expired') {
                            let expireTime = _.isObject(err.parsedBody) ? new Date(err.parsedBody.exp * 1000) : '';
                            next(new Error('Your token expired at ' + expireTime))
                        } else {  // Other error
                            next(new Error(err))
                        }
                    } else { // Valid token, get user info
                        // Init models
                        let { user: userModel } = await global.mysqlDb.repositories(['user'], global.env.defaultDb);

                        let query = {};

                        // Local account, find by its own Id
                        if (decoded.provider === 'local') {
                            Object.assign(query, {id: decoded.id})
                        } else { // Social account
                            Object.assign(query, {socialId: decoded.id})
                        }

                        // Check if this user existed
                        userModel.findOne(query).then((rs) => {
                            if (rs) {
                                // Check if this account is active
                                if (rs.status === '2') {
                                    // Only admin can use this namespace
                                    if(rs.role !== 'admin'){
                                        next(new Error('Authentication error'));
                                    }
                                    // Allow connect
                                    socket.handshake.query.userInfo = rs;
                                    next();
                                } else { // Inactive account
                                    next(new Error('Inactive account'));
                                }
                            } else { // Token doest not belong to this account
                                next(new Error('Authentication error'))
                            }
                        });
                    }
                });
            } else {
                next(new Error('Authentication error'));
            }
        })
        // Client connect listener
            .on('connection', async client => {
                this.logger.debug(`${socketIOTitle} Admin Client connected: SocketID = ${client.id}`);

                // Get language from client
                const lang = client.handshake.query.lang;

                // Add to socket client list
                const rs = await this.socketHelper.addSocket(this.clientList, client.handshake.query.userInfo, client);

                // Close this socket due to error
                if (rs.error) {
                    client.disconnect();
                    this.logger.debug(`${socketIOTitle} Disconnect admin client with SocketID = ${client.id} due to ${rs.errors} `);
                }

                // Client disconnect listener
                client.on('disconnect', async data => {
                    // Remove socket from list
                    let id = this.socketHelper.removeSocketFromList(this.clientList, client);


                    this.logger.debug(`Remove user ${id}`);

                    this.logger.debug(`${socketIOTitle} Admin Client disconnected: SocketID = ${client.id} `);

                });

                // Event listener for auction handler (for admin)
                // client.on('auctionHandler', async (receiveData, cb) => {
                //     let { type } = receiveData;
                //     try {
                //         switch (type){
                //             case 'pause': {
                //                 try {
                //                     let res = await AuctionController.pauseAuction();
                //
                //                     // Send for all user about this update
                //                     this.sendNewEventToAllConnectedUser('pause', 'No live auction is in progress at the moment.');
                //
                //                     cb(res);
                //                 } catch (e) {
                //                     global.logger.debug(`User ${client.handshake.query.userInfo._id} emit event ${type}`);
                //                     global.logger.debug(e);
                //
                //                     cb(e);
                //                 }
                //                 break;
                //             }
                //
                //             case 'resume': {
                //                 try {
                //                     let res = await AuctionController.resumeAuction();
                //
                //                     // Send for all user about this update
                //                     this.sendNewEventToAllConnectedUser('result', null);
                //
                //                     cb(res);
                //                 } catch (e) {
                //                     global.logger.debug(`User ${client.handshake.query.userInfo._id} emit event ${type}`);
                //                     global.logger.debug(e);
                //
                //                     cb(e);
                //                 }
                //                 break;
                //             }
                //
                //             case 'bidWarning': {
                //                 try {
                //                     // Send for all user about this update
                //                     this.sendNewEventToAllConnectedUser('bidWarning', 'You should place your bids quickly, as the article is about to be sold.');
                //
                //                     cb(null);
                //                 } catch (e) {
                //                     global.logger.debug(`User ${client.handshake.query.userInfo._id} emit event ${type}`);
                //                     global.logger.debug(e);
                //
                //                     cb(e);
                //                 }
                //                 break;
                //             }
                //
                //             case 'sellItem': {
                //                 try {
                //                     // Get the next item
                //                     let res = await AuctionController.sellItem();
                //
                //                     // Send for all user about this update
                //                     this.sendNewEventToAllConnectedUser('nextItem', res.data);
                //
                //                     cb(res);
                //                 } catch (e) {
                //                     global.logger.debug(`User ${client.handshake.query.userInfo._id} emit event ${type}`);
                //                     global.logger.debug(e);
                //
                //                     cb(e);
                //                 }
                //                 break;
                //             }
                //         }
                //     } catch (e) {
                //         global.logger.debug(`Player ${client.handshake.query.userInfo._id} emit event ${type}`);
                //         global.logger.debug(e);
                //
                //         cb(e);
                //     }
                // });
            });
    }

    /*///////////////////////////////////////////////////////////////
    /////                   START BASIC METHOD                 /////
    ///////////////////////////////////////////////////////////////*/

    // Send event to multi users
    private async sendNewEventToUsers(userIdArr: string[], eventName: string, data: any): Promise<any> {
        return await this.socketEmitter.emitEventToUserIdArr(userIdArr, eventName, data);
    }

    // Send event to multi rooms
    private sendNewEventToRooms(roomIdArr: string[], eventName: string, data: any) {
        this.socketEmitter.emitEventToRoomIdArr(roomIdArr, eventName, data);
    }

    // Send event to all connected users
    private sendNewEventToAllConnectedUser(eventName: string, data: any) {
        this.socketEmitter.emitEventToAllConnectedUser(eventName, data);
    }


    // Join a room
    private joinRoomById(userId: string, roomId: string): void {
        if (this.clientList[userId]) {
            // Get socket instance by user Id
            const {socket} = this.clientList[userId];

            // If this socket connection is opening
            if (socket) {
                socket.join(roomId, () => {
                    this.logger.info(`${socketIOTitle} Client ${userId} with socket Id: ${socket.id} has just joined room ${roomId} `);
                });
            }
        }
    };

    // Leave a room
    private leaveRoomById(userId: string, roomId: string): void {
        if (this.clientList[userId]) {
            // Get socket instance by user Id
            const {socket} = this.clientList[userId];

            socket.leave(roomId, () => {
                this.logger.debug(`${socketIOTitle} Client ${userId} with socket Id: ${socket.id} has just left room ${roomId} `);
            });
        }
    };

    // Force multi users leave on specific room
    private leaveRoomByUserIdArr(userIdArr: string[], roomId: string): void {
        this.socketHelper.leaveRoomByUserIdArr(userIdArr, roomId);
    };

    // Get all current socket rooms
    private getRoomList(): object {
        return this.io.sockets.adapter.rooms
    }

    // Check if specific use connected to socket or not
    public isConnected(userId: string): boolean {
        return !_.isEmpty(this.clientList[userId])
    }

    // Close socket by user Id
    public closeSocketByUserId(userId: string): void {
        if (this.clientList[userId]) {
            this.socketHelper.closeSocket(this.clientList[userId].socket)
        }
    }

    private sendNewEventToAllConnectedAdminAndEditor(eventName: string, data: any): void {
        this.socketEmitter.emitEventToAllConnectedAdmin(eventName, data);
        this.socketEmitter.emitEventToAllConnectedEditor(eventName, data);
    }

    // Send event to all connected admins
    private sendNewEventToAllConnectedAdmin(eventName: string, data: any): void {
        this.socketEmitter.emitEventToAllConnectedAdmin(eventName, data);
    }

    // Send event to all connected editors
    private sendNewEventToAllConnectedEditor(eventName: string, data: any): void {
        this.socketEmitter.emitEventToAllConnectedEditor(eventName, data);
    }

    // Update client's socket info
    public updateSocketClientInfo(userId: string, data: any): void {
        if (this.clientList[<string>userId]) {
            // Keep socket connection info
            data.socket = this.clientList[<string>userId].socket;
            this.clientList[<string>userId] = data;
        }

    }

    public getOnlineUser(): number{
        return Object.keys(this.clientList).length
    }


    /*///////////////////////////////////////////////////////////////
    /////                     END BASIC METHOD                  /////
    ///////////////////////////////////////////////////////////////*/
}
