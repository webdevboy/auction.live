import * as crypto from 'crypto';
import * as q from 'q';
import * as faker from 'faker';
import * as bcrypt from 'bcryptjs';
import * as moment from 'moment';
import * as _ from 'underscore';
import * as dotenv from 'dotenv';


const helpers = {
    assignObjectValueFromOtherObject: (desObj: any, srcObj: object): void => {
        Object.keys(srcObj).map((key) => {
            desObj[key] = srcObj[key];
        })
    },

    // Ge a random code, used for reset password
    getRandomCode: (): Promise<string> => {
        const defer = q.defer();

        crypto.randomBytes(20, (err, buf) => {
            if (err) {
                defer.reject(err);
            } else {
                defer.resolve(buf.toString('hex'));
            }
        });

        return defer.promise;
    },

    // Generate password reset url to send in email for Admin
    getAdminResetPasswordUrl: async (webBaseUrl: string, code: string, tenantCode?: string): Promise<string> => {
        return `${webBaseUrl}?code=${code}`;
    },

    getDateMilliSecs: (date?: Date): number => {
        return date ? new Date(date).getTime() : new Date().getTime();
    },

    minuteToMilliSecs: (minute: number): number => {
        return minute * 60000;
    },

    // Generate OTP code
    generateVerifyCode: (length = 6): number => {
        return faker.random.number({ min: 100000, max: 999999, precision: length })
    },

    // Generate hash password
    generateHashPassword: async (password: string): Promise<string> => {
        // Generate a salt for hash string
        const salt = await bcrypt.genSaltSync(parseInt(global.env.hashSalt));

        return await bcrypt.hashSync(password, salt);
    },

    generateHashString: async (text: string): Promise<string> => {
        dotenv.config();

        console.log(`Hash ${text}`)
        // Generate a salt for hash string
        const salt = await bcrypt.genSaltSync(parseInt(process.env.HASH_SALT));

        return await bcrypt.hashSync(text, salt);
    },

    // Stripe '0' number at the first string, used for phone number
    stripeZeroOut: (string: string): string => {
        return string.replace(/^0+/, '')
    },

    // Get random integer number
    getRandomInt(max: number): number {
        return Math.floor(Math.random() * Math.floor(max)) + 1;
    },

    // Get random integer number including zero
    getRandomIntWithZero(max: number): number {
        return Math.floor(Math.random() * Math.floor(max));
    },

    // Calculate years from current datetime
    calculateDate(date: string): number {
        return moment().diff(moment(date, 'MM/DD/YYYY'), 'years');
    },

    // Convert array of objects to array of strings
    returnPlatArray(arr: object[], key: string) {
        return arr.map(item => {
            return item[key]
        })
    },

    // Replace string item from nested array by null value
    removeItemFromNestArray(arr: string[][], string: string): any {
        return arr.map((nestedArr: string[]) => {
            return nestedArr.map((item) => {
                if (item && item.toString() === string.toString()) {
                    return null
                }else{
                    return item
                }
            })
        })
    },

    // Merge nested array without null value inside
    mergeNestedArrayWithoutNullValue(arr: string[][]): any {
        return [].concat.apply([], arr).filter((item) => {
            return item !== null
        });
    },

    // Insert value from an value array to 2d Id array
    insertValueIntoCorrespondingArrById(valueArr: any[], idArr: any[][]): void {
        idArr.map((nestedArr, index) => {
            nestedArr.map((id, index2) => {
                valueArr.map((value, index3) => {
                    if (id && value._id.toString() === id.toString()) {
                        idArr[index][index2] = value;
                    }
                })
            })
        });
    },

    // Get number of not null element in 2d array
    getCountOfNotNullItemFromNestArray(arr: string[][]): number {
        let count = 0;
        arr.map((nestedArr: string[]) => {
            nestedArr.map((item) => {
                if (item) {
                    count++
                }
            })
        });
        return count;
    },

    // Assign value to the nearest empty slot in 2d array
    assignValueToNestArray(arr: string[][], value: string): void {
        arr.every((nestedArr) => {
            return nestedArr.every((item, index) => {
                if (item === null) {
                    nestedArr[index] = value;
                    return false // Stop
                } else {
                    return true // Keep browser array
                }
            })
        })
    },

    // Convert value array to regex array
    convertToRegexArray(arr: string[]): RegExp[] {
        return arr.map((item) => {
            // Remove special characters
            item = item.replace(/^0+(?=\d)/, '').replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
            return new RegExp(item, 'i')
        })
    },

    // Get array by object field
    getArrayByField(arr: object[], field: string): string[] {
        return _.pluck(arr, field);
    },

    // Get element matched from arr
    getElementNotMatchedRegex(regexArr: RegExp[], targetArr: string[]): RegExp[] {
        return regexArr.filter((regex) => {
            let rs = targetArr.filter((item) => {
                return item.match(regex) // Get item match with regex
            });

            return rs.length === 0 // Item is not matched is that one we want to find
        });

    },

    // Convert RegExp arr to string
    convertRegExpToString(arr: RegExp[]): object[] {
        return arr.map((regex) => {
            return {
                fullPhoneNumber: regex.source,
                availableInGame: false,
                isFriend: false,
                smsContent: 'Download your app at: www.google.com',
                isSentFriendRequest: false
            }
        })
    },

    // Merge 2 arrays and eliminate duplicated element
    mergeAndEliminateDupItem(arr1: string[], arr2: string[]): string[] {
        return _.union(arr1, arr2);
    },

    paginateAnArray(arr: object[], size: number, page: number): object {
        let skip = 0;
        let limit = 0;
        let data = null;

        // Limit item
        if (size && +size > 0) {
            // Limit page with size
            if (page && +page > 0) {
                skip = (+page) * (+size);
                limit = (+size)
            } else { // Limit page with default
                limit = (+size)
            }
        } else {
            if (page && +page > 0) {
                return {
                    currentPage: +page,
                    totalPage: 1,
                    data: []
                };
            }
        }

        // Calculate total page
        let totalPage = +size > 0 ? Math.ceil(arr.length / +size) : 1;

        if (skip === 0 && limit === 0) {
            data = arr
        } else {
            data = arr.slice(skip, (skip + 1) * limit);
        }


        return {
            currentPage: +page ? +page : 0,
            totalPage,
            data
        }
    },

    // Find object Id in array
    findObjectIdInArray(arr: string[], value): number{
        let slotIndex = -1;
        arr.every((item, index) => {
            if (item && item.toString() === value.toString()) {
                slotIndex = index;
                return false // Stop
            } else {
                return true // Keep browser array
            }
        });

        return slotIndex;
    },

    // Get nol null item from 2d Arr
    getNotNullItemFrom2dArr(arr: string[]): string[]{
       return arr.filter(item => item !== null)
    },

	// Get full original url from request
    getOriginalUrl(req: any, path?: string): string {
        if (path) {
            return `${req.protocol}://${req.get('host')}${path}`;
        }

        return `${req.protocol}://${req.get('host')}${req.originalUrl}`;
    },

    async compareHashString (string1: string, string2: string): Promise<boolean>{
        return await bcrypt.compare(string1, string2)
    }

};


export default helpers
