# Auction.Live Documentation

### Contents
* [System Architecture](architecture/system_architecture.md)
* [Configuration Files](implementation/config.md)
* [Data Model](implementation/data-model/data_model.md)
* [Spec Sheet](specs/spec_sheet.pdf)