# System Architecture

## High Level Connectivity Architecture
![High Level Architecture](assets/connectivity_architecture-high_level.jpg)<br>
TODO: Description

### Connection Details
![Connection Details](assets/connectivity_architecture-details.jpg)<br>
TODO: Description

## Alternative Deployment: Node via Plesk
When deploying using a Plesk controlled Node runtime, the reverse proxy and certificate delivery are managed by the Plesk setup - the NGINX box in the architecture sketch does not apply in that case. 