# System configuration overview

### Database
(is this for WP database or?)
- hostname
- port
- username
- password

### App settings
- Admin language
- Frontend language
- Currency
- Logo
- Media Server URL (this one should be reviewed)
- Timezone

### Mail settings
- server
- port
- username
- password
- from name

### Mail content
- Translatable email content (HTML + CSS)

We are going to use JSON fields in a table for storing these. Maria DB has some support for JSON fields: [https://mariadb.com/kb/en/json-data-type/](https://mariadb.com/kb/en/json-data-type/)