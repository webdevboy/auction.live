import axios, { AxiosError, AxiosResponse, AxiosRequestConfig } from 'axios'

const getClient = (baseUrl?: string) => {
  const options: AxiosRequestConfig = {
    baseURL: baseUrl
  }

  const client = axios.create(options)

  return client
}

class ApiClient {
  client: any

  constructor (baseUrl = process.env.VUE_APP_SERVER) {
    this.client = getClient(baseUrl)
  }

  get (url: any, conf = {}) {
    return this.client.get(url, conf)
      .then((response: AxiosResponse) => Promise.resolve(response))
      .catch((error: AxiosError) => Promise.reject(error))
  }

  delete (url: any, conf = {}) {
    return this.client.delete(url, conf)
      .then((response: AxiosResponse) => Promise.resolve(response))
      .catch((error: AxiosError) => Promise.reject(error))
  }

  head (url: any, conf = {}) {
    return this.client.head(url, conf)
      .then((response: AxiosResponse) => Promise.resolve(response))
      .catch((error: AxiosError) => Promise.reject(error))
  }

  options (url: any, conf = {}) {
    return this.client.options(url, conf)
      .then((response: AxiosResponse) => Promise.resolve(response))
      .catch((error: AxiosError) => Promise.reject(error))
  }

  post (url: any, data = {}, conf = {}) {
    return this.client.post(url, data, conf)
  }

  put (url: any, data = {}, conf = {}) {
    return this.client.put(url, data, conf)
      .then((response: AxiosResponse) => Promise.resolve(response))
      .catch((error: AxiosError) => Promise.reject(error))
  }

  patch (url: any, data = {}, conf = {}) {
    return this.client.patch(url, data, conf)
      .then((response: AxiosResponse) => Promise.resolve(response))
      .catch((error: AxiosError) => Promise.reject(error))
  }
}

export { ApiClient }

/**
 * Base HTTP Client
 */
export default {
  // Provide request methods with the default base_url
  get (url: any, conf = {}) {
    return getClient().get(url, conf)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.reject(error))
  },

  delete (url: any, conf = {}) {
    return getClient().delete(url, conf)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.reject(error))
  },

  head (url: any, conf = {}) {
    return getClient().head(url, conf)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.reject(error))
  },

  options (url: any, conf = {}) {
    return getClient().options(url, conf)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.reject(error))
  },

  post (url: any, data = {}, conf = {}) {
    return getClient().post(url, data, conf)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.reject(error))
  },

  put (url: any, data = {}, conf = {}) {
    return getClient().put(url, data, conf)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.reject(error))
  },

  patch (url: any, data = {}, conf = {}) {
    return getClient().patch(url, data, conf)
      .then(response => Promise.resolve(response))
      .catch(error => Promise.reject(error))
  }
}
