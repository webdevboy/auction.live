import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import './global/style.scss';
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'
import moment from 'moment';
import GetTextPlugin from 'vue-gettext'
import translations from "./translations/translations.json";

// Translate plugin
Vue.use(GetTextPlugin, {
  availableLanguages: {
    en: 'English',
    de: 'German',
  },
  defaultLanguage: 'de',
  translations: translations,
  silent: true,
})

Vue.prototype.moment = moment
Vue.use(Vuetify, {
  iconfont: 'mdiSvg',
  theme: {
    themes: {
      light: {
        primary: '#90C143',
        secondary: '#b0bec5',
        anchor: '#8c9eff',
      },
    },
  },
})
const opts = {}
Vue.config.productionTip = false

new Vue({
  vuetify: new Vuetify(opts),
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
