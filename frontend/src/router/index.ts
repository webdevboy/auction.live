import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '@/views/login/index.vue'
import registration from '@/views/registration/index.vue'
import resetPassword from '@/views/reset-password/index.vue'
import auction from '@/views/auction/index.vue'
import auctionAdmin from '@/views/auction-admin/index.vue'

Vue.use(VueRouter);

const routes = [
  {
  path: '/',
  name: 'login',
  component: login,
  meta: {
    requiresAuth: false
  }
},
  {
    path: '/auction',
    name: 'auction',
    component: auction,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/auction-admin',
    name: 'auctionAdmin',
    component: auctionAdmin,
    meta: {
      requiresAuth: true
    }
  },
{
  path: '/login',
  name: 'login',
  component: login,
  meta: {
    requiresAuth: false
  }
},
{
  path: '/registration',
  name: 'registration',
  component: registration,
  meta: {
    requiresAuth: false
  }
},
{
  path: '/resetPassword',
  name: 'resetPassword',
  component: resetPassword,
  meta: {
    requiresAuth: false
  }
}];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

// Middleware

router.beforeEach((to, from, next) => {
  let token = localStorage.getItem('token');

  if (to.matched.some(record => record.meta.requiresAuth)) {
    // If component require authentication but token is invalid
    if (token == null || token === undefined || token === '') {
      // Back to login
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    } else {
      // Process to page
      next()
    }
  } else {
    // Continue with current page
    next()
  }
});

export default router
