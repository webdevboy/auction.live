import Vue from 'vue';
import apiRequests from './apiRequests';
import router from '../router';
import axios from 'axios';
import { getAuthToken, setAuthToken } from '../utils'

const actions = {
  // Change global app state
  changeGlobalState: ({commit}: any, object: any): any => {
    commit('changeGlobalState', object);
  },

  logout: ({commit}: any) => {
    // Remove token from header
    axios.defaults.headers.common['Authorization'] = '';

      // Remove token
    setAuthToken('')

    // Update profile
    commit('changeGlobalState', {key: 'userProfile', value: {}});

    // Back to login page
    router.push('/login');
  },

  // Get user profile
  getUserProfile: async ({commit}: any) => {
    try {
      // Show loading
      commit('changeGlobalState', {key: 'loading', value: true});
      let result = await apiRequests.getUserProfile();

      // Cant connect to backend or no internet
      if (!result) {
        // Show loading
        commit('changeGlobalState', {key: 'loading', value: false});

        return false;
      }

      let {data} = result;

      // Error happens
      if (data.error) {
        // Hide loading
        commit('changeGlobalState', {key: 'loading', value: false});

        return false

      } else { // Success
        // Hide loading
        commit('changeGlobalState', {key: 'loading', value: false});

        // Change system language
        if(data.data.language === 'english'){
          axios.defaults.headers.common['lang'] = 'en';
          Vue.config.language = 'en'
        }else{ // Default is german
          axios.defaults.headers.common['lang'] = 'de';
          Vue.config.language = 'de'
        }

        // Update profile
        commit('changeGlobalState', {key: 'userProfile', value: data.data});

        if(data.data.role === 'admin'){
          if(router.currentRoute.path !== '/auction-admin'){
            // Go to auction page
            router.push('/auction-admin');
          }
        }else{
          // Redirect to auction page if user still didnt join yet
          if(router.currentRoute.path !== '/auction'){
            // Go to auction page
            router.push('/auction');
          }
        }



        return true
      }
    }catch (e) {

    }
  },

  // Get notification list
  getNotification: async ({commit}: any) => {
    try {
      // Show loading
      commit('changeGlobalState', {key: 'loading', value: true});
      let result = await apiRequests.getNotification();

      // Cant connect to backend or no internet
      if (!result) {
        // Show loading
        commit('changeGlobalState', {key: 'loading', value: false});

        return false;
      }

      let {data} = result;

      // Error happens
      if (data.error) {
        // Hide loading
        commit('changeGlobalState', {key: 'loading', value: false});

        return false

      } else { // Success

        // Hide loading
        commit('changeGlobalState', {key: 'loading', value: false});

        // Update profile
        commit('changeGlobalState', {key: 'notificationList', value: data.data});

        return data.data
      }
    }catch (e) {

    }
  },

  // Mark read notification
  markReadNotification: async ({commit}: any, id: number) => {
    try {
      // Show loading
      commit('changeGlobalState', {key: 'loading', value: true});
      let result = await apiRequests.markReadNotification(id);

      // Cant connect to backend or no internet
      if (!result) {
        // Show loading
        commit('changeGlobalState', {key: 'loading', value: false});

        return false;
      }

      let {data} = result;

      // Error happens
      if (data.error) {
        // Hide loading
        commit('changeGlobalState', {key: 'loading', value: false});

        return false

      } else { // Success

        // Hide loading
        commit('changeGlobalState', {key: 'loading', value: false});

        return data.data
      }
    }catch (e) {

    }
  },

  // Update user profile
  updateUserProfile: async ({commit}: any, userData: any) => {
    try {
      // Show loading
      commit('changeGlobalState', {key: 'loading', value: true});

      let result = await apiRequests.updateUserProfile(userData);

      if (!result) {
        // Show loading
        commit('changeGlobalState', {key: 'loading', value: false});

        return false
      }

      let {data} = result;

      // Error happens
      if (data.error) {
        // Hide loading and Show error
        commit('changeGlobalState', [
          {key: 'loading', value: false},
          {key: 'error', value: true},
          {key: 'errorMessage', value: data.errors[0].errorMessage},
        ]);

        return false

      } else { // Success
        // Hide loading
        commit('changeGlobalState', {key: 'loading', value: false});

        // Update profile
        commit('changeGlobalState', {key: 'userProfile', value: data.data});

        return true
      }
    }catch (e) {

    }
  }
};

export default actions;
