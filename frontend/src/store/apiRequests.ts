import axios from 'axios';

const apiRequests = {

  getUserProfile: async (): Promise<any> =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/user/profile`;

      axios
        .get(url)
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    }),

  getNotification: async (): Promise<any> =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/user/notification`;

      axios
        .get(url)
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    }),

  markReadNotification: async (id: number): Promise<any> =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/user/notification/markRead/${id}`;

      axios
        .put(url)
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    }),

    updateUserProfile: async (data: any): Promise<any> =>
      new Promise((resolve, reject) => {
        let host = process.env.VUE_APP_SERVER;
        const url = `${host}/api/v1/user/profile`;

        axios
          .put(url, data)
          .then(rs => {
            resolve(rs);
          })
          .catch(err => {
            resolve(err.response);
          });
      })
};

export default apiRequests;
