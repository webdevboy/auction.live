import Vue from 'vue'
import Vuex from 'vuex'
import login from '../views/login/store'
import registration from '../views/registration/store'
import resetPassword from '../views/reset-password/store';
import auction from '../views/auction/store';
import actions from './actions';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    userProfile: {},
    token: '',
    error: false,
    errorMessage: '',
    socketInstance: null,
    notificationList: [],
    showBottomNavigation: true // Show bottom mobile notification bar
  },
  mutations: {
    // changeGlobalState(state: any, object: any){
    //   if (typeof object.value === 'object'){
    //     state[object.key] = {...state[object.key], ...object.value}
    //   }else{
    //     state[object.key] = object.value
    //   }
    // }
    changeGlobalState(state: any, object: any){
      // Array
      if(object.length > 0){
        object.map((item: any, index: number)=>{
          state[item.key] = item.value
        })

      }else{
        state[object.key] = object.value
      }

    }
  },
  actions,
  modules: {
    login,
    registration,
    resetPassword,
    auction
  }
})
