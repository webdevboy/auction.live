
export function getAuthToken(): string{
  return <string>window.localStorage.getItem('token')
}

export function setAuthToken(token: string) {
  window.localStorage.setItem('token', token);
}

export function removeAuthToken() {
  window.localStorage.removeItem('token');
}
