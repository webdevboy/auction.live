import io from 'socket.io-client';

const SocketClient = {
  connectSocket: function(host: string, query: any) {
    return io(`${host}/users`, {query});
  }
};

export default SocketClient
