import apiRequests from './apiRequests';

export const changeState = async ({ commit }: any, data: any) => {
  // Update current auction
  commit('changeState', data);
};

export const getCurrentActiveAuction = async ({ commit }: any) => {
  try {
    let result = await apiRequests.getActiveAuction();

    // Cant connect to backend or no internet
    if (!result) {
     return null
    }

    let {data} = result;

    // Error happens
    if (data.error) {
     return data

    } else { // Success

      // Update current auction
      commit('changeState', {key: 'currentAuction', value: data.data});

      return data
    }
  }catch (e) {

  }
};

export const startAuction = async ({ commit }: any, auctionId: number) => {
  try {
    // Show loading
    commit('changeGlobalState', {key: 'loading', value: true}, { root: true });

    let result = await apiRequests.startAuction(auctionId);

    // Cant connect to backend or no internet
    if (!result) {
      return null
    }

    let {data} = result;

    // Error happens
    if (data.error) {
      // Hide loading
      commit('changeGlobalState', {key: 'loading', value: false}, { root: true });

      return data

    } else { // Success
      // Get new data
      let result = await apiRequests.getActiveAuction();

      // Update current auction
      commit('changeState', {key: 'currentAuction', value: result.data.data});

      commit('changeGlobalState', {key: 'loading', value: false}, { root: true });

      return data
    }
  }catch (e) {

  }
};

export const jumpToArticle = async ({ commit }: any, articleId: number) => {
  try {
    // Show loading
    commit('changeGlobalState', {key: 'loading', value: true}, { root: true });

    let result = await apiRequests.jumpToArticle(articleId);

    // Cant connect to backend or no internet
    if (!result) {
      return null
    }

    let {data} = result;

    // Error happens
    if (data.error) {
      // Hide loading
      commit('changeGlobalState', {key: 'loading', value: false}, { root: true });

      return data

    } else { // Success
      // Get new data
      let result = await apiRequests.getActiveAuction();

      // Update current auction
      commit('changeState', [{key: 'currentAuction', value: result.data.data}]);
      // commit('updateArticleStatus', {id: articleId, data: data.data});


      commit('changeGlobalState', {key: 'loading', value: false}, { root: true });

      return data
    }
  }catch (e) {

  }
};

export const doAction = async ({ commit }: any, payload: any) => {
  try {
    // Show loading
    commit('changeGlobalState', {key: 'loading', value: true}, { root: true });

    let result = await apiRequests.doAction(payload);

    // Cant connect to backend or no internet
    if (!result) {
      return null
    }

    let {data} = result;

    // Error happens
    if (data.error) {
      // Hide loading and Show error
      commit('changeGlobalState', [
        {key: 'loading', value: false},
        {key: 'error', value: true},
        {key: 'errorMessage', value: data.errors[0].errorMessage},
      ], { root: true });

      return data

    } else { // Success
      // Get new data
      let result = await apiRequests.getActiveAuction();

      // Update current auction
      commit('changeState', {key: 'currentAuction', value: result.data.data});

      commit('changeGlobalState', {key: 'loading', value: false}, { root: true });

      return data
    }
  }catch (e) {

  }
};

export const doBid = async ({ commit }: any, uploadData: any) => {
  try {
    commit('changeGlobalState', {key: 'loading', value: true}, { root: true });

    let result = await apiRequests.doBid(uploadData);

    if (!result) {
      // Show loading
      commit('changeGlobalState', {key: 'loading', value: false}, { root: true });

      return false
    }

    let {data} = result;

    // Error happens
    if (data.error) {
      // Hide loading and Show error
      commit('changeGlobalState', [
        {key: 'loading', value: false},
        {key: 'error', value: true},
        {key: 'errorMessage', value: data.errors[0].errorMessage},
      ], { root: true });

      return false

    } else { // Success
      // Hide loading
      commit('changeGlobalState', {key: 'loading', value: false}, { root: true });

      // Update current auction
      commit('changeState', {key: 'currentAuction', value: data.data});

      return true
    }
  }catch (e) {

  }
};



