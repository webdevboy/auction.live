import axios from 'axios';

const apiRequests = {
  getActiveAuction: async (): Promise<any> =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/auction/active`;

      axios
        .get(url)
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    }),

  doBid: async (data: any): Promise<any> =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/auction/doBid/${data.auctionId}/${data.articleId}`;

      axios
        .put(url, {bid: data.bid})
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    }),

  startAuction: async (id: number): Promise<any> =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/admin/auction/start/${id}`;

      axios
        .put(url)
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    }),

  doAction: async (data: any): Promise<any> =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/admin/auction/doAction`;

      axios
        .put(url, data)
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    }),

  jumpToArticle: async (id: number): Promise<any> =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/admin/auction/jumpToArticle/${id}`;

      axios
        .put(url)
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    })
};

export default apiRequests;
