import * as actions from './actions'
import * as _ from 'lodash';
import moment from 'moment';
import Vue from 'vue';

const loadingMessage = {
  noLive: 'No live auction has been started',
  paused: 'The auction has been paused. Please wait.',
  connection: 'Your connection has been interrupted. Trying to reconnect... ',
  suspended: 'The auction has been temporarily suspended due to administrator connection issue. Please wait.'
};

const state = {
  currentAuction: {
  },
  loadingModal: true,
  loadingType: 'noLive',
  selectedArticle: null,
  showPushNotiTooltip: false,
  stopLoadingModal: false,
  jumpLoadingModal: false,
  reauctionLoadingModal: false,
  articleList: []
};

const mutations = {
  changeState(state: any, object: any){
    // Array
    if(object.length > 0){
      object.map((item: any, index: number)=>{
        state[item.key] = item.value
      })

    }else{
      if(typeof object.value === 'object'){
        state[object.key] = {...state[object.key],...object.value}
      }else{
        state[object.key] = object.value
      }

    }
  },
  updateArticleStatus(state: any, data: any){
    console.log(data)
    let index = _.findIndex(state.currentAuction.articleList, ['id', data.id]);

    console.log(`Update at index ${index}`)

    Vue.set(state.currentAuction.articleList, index, data.data)
  }
};

const getters = {
  formattedCurrentAuction: (state: any) => {
    if(_.isEmpty(state.currentAuction.articleList)){
      return {...state.currentAuction, articleList: []}
    }else{
      return state.currentAuction;
    }
  },

  // Current article to be sold
  ongoingArticle: (state: any) => {
    if(_.isEmpty(state.currentAuction.ongoingArticle)){
      return {}
    }else{
      if(state.selectedArticle === null){
        return state.currentAuction.ongoingArticle
      }else{
        return state.currentAuction.articleList[state.selectedArticle]
      }

    }
  },

  // Get current highest bid
  currentHighestBid: (state: any) => {
    if(_.isEmpty(state.currentAuction.ongoingArticle)) {
      return {value: 'N/A', id: null}
    }else{ // If this is no bid yet, return the limit step
      console.log('get curent')
      return state.currentAuction.highestBid ?
        {value: state.currentAuction.highestBid.value, id: state.currentAuction.highestBid.user_id } :
        {value: 0, id: null, articleValue: state.currentAuction.ongoingArticle.limit }
    }
  },

  expectValue: (state: any) => {
    return state.currentAuction.expectPrice
  },

  currentStepValue: (state: any) => {
    return state.currentAuction.bidStep ? state.currentAuction.bidStep : {limit: 0}
  },

  userData: (state: any) => {
    return state.currentAuction.userData
  },

  // Bid history
  bidHistory(state: any){
    return state.currentAuction.bidHistory ? state.currentAuction.bidHistory.map((item: any,index: number)=>{
      item.created_at = moment(new Date(item.created_at)).format('hh:mm:ss')
      item.type = item.type === 'live' ? 'Live' : 'Hall';
      item.bid = '€'+item.bid;
      return item;
    }) : []
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
