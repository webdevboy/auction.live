import apiRequests from './apiRequests';

export const loginForm = async ({ commit }: any, account:any) => {
  try{
    return await apiRequests.signIn(account.email, account.password)
  }catch (e) {
    throw(e)
  }

};
