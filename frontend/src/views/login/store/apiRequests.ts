import axios from 'axios';

const apiRequests = {
  signIn: async (email: string, password: string) =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/auth/signIn`;

      let postData = {
        email,
        password
      };

      axios
        .post(url, postData)
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    })
};

export default apiRequests;
