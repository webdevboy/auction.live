import * as actions from './actions'

const state = {
  account: null,
  accessToken: localStorage.getItem('user')
}
const mutations = {
  login (state: any, token: any) {
    state.account = null
    state.accessToken = token
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
