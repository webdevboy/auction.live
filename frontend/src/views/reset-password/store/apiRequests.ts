import axios from 'axios';

const apiRequests = {
  resetPassword: async (data: any) =>
    new Promise((resolve, reject) => {
      let host = process.env.VUE_APP_SERVER;
      const url = `${host}/api/v1/auth/requestResetPassword`;

      axios
        .post(url, data)
        .then(rs => {
          resolve(rs);
        })
        .catch(err => {
          resolve(err.response);
        });
    })
};

export default apiRequests;
