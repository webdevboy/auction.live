const fs = require('fs')

module.exports = {
  chainWebpack: config => {
    config.module.rules.delete('eslint')
  },
  outputDir: process.env.OUTPUT_DIR || '../backend/public'
}
