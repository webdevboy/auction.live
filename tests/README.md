# Ensure you clear the required database tables and create required users
You can use initdb.sql in this directory to do so.

# Test Users
### Admin
email: admin@test.com
pw: admin123
first name: AdminFN
last name: AdminLN

### Normal User
email: user@test.com
pw: admin123
first name: UserFN
last name: UserLN

