describe('Auction API', () => {

  let token
  let email = 'admin@test.com'
  let password = 'admin123'

  it('Sign in as admin', () => {
    let req = {
      email: email,
      password: password
    }
    
    cy.request({
      method: 'POST',
      url: '/api/v1/auth/signIn',
      body: req,
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data
      
      expect(error).to.eq(false)
      expect(data).to.have.all.keys('id', 'first_name', 'last_name', 'email', 'role', 'last_login', 'status', 'doi', 'created_at', 'updated_at', 'token')
      expect(data.email).to.eq(req.email)
      token = data.token
    })
  })

  it('List auctions empty', () => {
    cy.request({
      method: 'GET',
      url: '/api/v1/admin/auction/list',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data

      expect(error).to.eq(false)
      expect(data).to.have.length(0)
    })
  })

  it('Ensure list auction requires authentication', () => {
    cy.request({
      method: 'GET',
      url: '/api/v1/admin/auction/list',
      failOnStatusCode: false
    })
    .then((response) => {
      let error = response.body.error

      expect(error).to.eq(true)
      expect(response.status).to.eq(401)
    })
  })

  it('Create and get auction', () => {
    // let auction 
    cy.request({
      method: 'POST',
      url: '/api/v1/admin/auction',
      body: {
        active_until: '2021-02-02 21:00'
      },
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data

      expect(error).to.eq(false)
      expect(data).to.have.all.keys('code', 'active_until', 'created_by', 'id', 'is_active', 'status', 'created_at', 'updated_at')

      expect(data.is_active).to.eq(0)
      expect(data.status).to.eq('preparing')
      expect(data.created_by).to.eq(1)
      
      let auction = data
      cy.request({
        method: 'GET',
        url: `/api/v1/admin/auction/${auction.id}`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then((response) => {
        let error = response.body.error
        let data = response.body.data
  
        expect(error).to.eq(false)
        expect(data.id).to.eq(auction.id)
        expect(data.code).to.eq(auction.code)
        expect(data.status).to.eq(auction.status)
      })
    })

  })

  it('List auctions with content', () => {
    cy.request({
      method: 'GET',
      url: '/api/v1/admin/auction/list',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data

      expect(error).to.eq(false)
      expect(data).to.have.length(1)

      let auction = data[0]
      expect(auction).to.have.all.keys('code', 'active_until', 'created_by', 'id', 'is_active', 'status', 'created_at', 'updated_at')
    })
  })

})