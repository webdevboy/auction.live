describe('User API', () => {

  let token
  let email = 'admin@test.com'
  let password = 'admin123'

  it('Sign in as admin', () => {
    let req = {
      email: email,
      password: password
    }
    
    cy.request({
      method: 'POST',
      url: '/api/v1/auth/signIn',
      body: req,
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data
      
      expect(error).to.eq(false)
      expect(data).to.have.all.keys('id', 'first_name', 'last_name', 'email', 'role', 'last_login', 'status', 'doi', 'created_at', 'updated_at', 'token')
      expect(data.email).to.eq(req.email)
      token = data.token
    })
  })

  it('List users', () => {
    cy.request({
      method: 'GET',
      url: '/api/v1/admin/user/list',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data

      expect(error).to.eq(false)
      expect(data).to.have.length(1)

      // Get user in response
      let user = data[0]
      expect(user).to.have.keys('id', 'first_name', 'last_name', 'email', 'role', 'last_login', 'status', 'doi', 'created_at', 'updated_at')
      expect(user.id).to.eq(2)
      expect(user.first_name).to.eq('UserFN')
      expect(user.last_name).to.eq('UserLN')
      expect(user.email).to.eq('user@test.com')
      expect(user.role).to.eq('user')
      expect(user.status).to.eq('0')
    })
  })

  it('Activate user', () => {
    cy.request({
      method: 'PUT',
      url: '/api/v1/admin/user/activate/2',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data

      expect(error).to.eq(false)
      expect(data).to.eq('Success')
    })

    // Check if status has been updated
    cy.request({
      method: 'GET',
      url: '/api/v1/admin/user/list',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data

      expect(error).to.eq(false)
      expect(data).to.have.length(1)

      // Get user in response
      let user = data[0]
      expect(user.status).to.eq('2')
    })
  })

  it('Update user profile', () => {    
    // Update email
    cy.request({
      method: 'PUT',
      url: '/api/v1/admin/user/2',
      body: {
        email: 'user2@test.com'
      },
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data

      expect(error).to.eq(false)
      expect(data).to.eq('Success')
    })

    // Check if email has been updated
    cy.request({
      method: 'GET',
      url: '/api/v1/admin/user/list',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data

      expect(error).to.eq(false)
      expect(data).to.have.length(1)

      // Get user in response
      let user = data[0]
      expect(user.email).to.eq('user2@test.com')
    })
  })

})
