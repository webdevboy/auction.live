describe('Authentication API', () => {

  let code
  let email = 'test@example.com'
  let password = 'testpw'

  it('Request signup by email', () => {
    let req = {
      email: email
    }
    
    cy.request({
      method: 'POST',
      url: '/api/v1/auth/requestSignUpByEmail',
      body: req,
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data
      
      expect(error).to.eq(false)
      expect(data).to.have.all.keys('code', 'type', 'expired_at', 'email_request', 'id', 'valid', 'created_at', 'updated_at')
      expect(data.email_request).to.eq(req.email)
      
      code = data.code
    })
    
  })
  
  it('Sign up with code', () => {
    let req = {
      first_name: 'TestFN',
      last_name: 'TestLN',
      email: email,
      password: password,
      confirmPassword: password,
      code: code,
    }
    
    cy.request({
      method: 'POST',
      url: '/api/v1/auth/signUp',
      body: req,
      form: true,
    })
    .then((response) => {
      let error = response.body.error
      let data = response.body.data
      
      expect(error).to.eq(false)
      expect(data).to.have.all.keys('first_name', 'last_name', 'email', 'code', 'last_login', 'role', 'id', 'status', 'doi', 'created_at', 'updated_at')
      
      // Check for correct input mirroring
      expect(data.first_name).to.eq(req.first_name)
      expect(data.last_name).to.eq(req.last_name)
      expect(data.email).to.eq(req.email)
      expect(data.code).to.eq(req.code)

      // Check for correct defaults
      expect(data.role).to.eq('user')
      expect(data.doi).to.eq(false)
    })
  })

  it('Sign in as user', () => {
    let req = {
      email: email,
      password: password,
    }
    
    cy.request({
      method: 'POST',
      url: '/api/v1/auth/signIn',
      body: req,
      failOnStatusCode: false,
    })
    .then((response) => {
      // Expect unauthorized with error code 6000 and corresponding error message
      expect(response.status).to.eq(401)
      expect(response.body.error).to.eq(true) 
      expect(response.body.errors[0].errorCode).to.eq(6000)
      expect(response.body.errors[0].errorMessage).to.eq('This user account has not yet been unlocked for live bidding')
    })
  })

})