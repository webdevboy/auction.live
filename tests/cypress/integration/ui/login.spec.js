describe('Login UI', () => {
  
  it('Returns 401 when not authenticated', () => {
    cy.visit('/')
    
    cy.get('body')
      .should('be.visible')

    // cy.contains('Please authorize to start biding') // fails
    cy.contains('Please Authorize to start biding')
    cy.contains('Email')
    cy.contains('Password')
    cy.contains('Forgot password?')
  })

})