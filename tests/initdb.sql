DELETE FROM auction_live.auction_users WHERE id > 0;
DELETE FROM auction_live.auctions WHERE id > 0;
DELETE FROM auction_live.users WHERE id > 0;

INSERT INTO `auction_live`.`users`
(`id`,
`first_name`,
`last_name`,
`email`,
`password`,
`role`,
`last_login`,
`status`,
`doi`,
`created_at`,
`updated_at`)
VALUES
(1,
'AdminFN',
'AdminLN',
'admin@test.com',
'$2a$10$A9L7Gb8pFDtANeU4fWasMuvC.SVFw8NwhwN3h5nRRjPFxdY3Qw8M6',
'admin',
'2020-07-31 15:44:07',
'2',
0,
'2020-07-31 13:44:07.247949',
'2020-07-31 13:44:07.247949');

INSERT INTO `auction_live`.`users`
(`id`,
`first_name`,
`last_name`,
`email`,
`password`,
`role`,
`last_login`,
`status`,
`doi`,
`created_at`,
`updated_at`)
VALUES
(2,
'UserFN',
'UserLN',
'user@test.com',
'$2a$10$A9L7Gb8pFDtANeU4fWasMuvC.SVFw8NwhwN3h5nRRjPFxdY3Qw8M6',
'user',
'2020-07-31 15:44:07',
'0',
0,
'2020-07-31 13:44:07.247949',
'2020-07-31 13:44:07.247949');